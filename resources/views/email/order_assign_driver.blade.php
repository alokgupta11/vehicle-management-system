@component('mail::message')

<p>Dear <b>{{$user->first_name}} {{$user->last_name}}!</b></p>

<p>A New Order <b>#{{$order->order_uuid}}</b> has been Assigned to you.</p>
<p>Please login into your account for more details.</p>

<p>If any queries reach us @ <b>Phone: </b>{{env('SUPPORT_CONTACT')}}</b> <b>Email: </b>{{env('SUPPORT_EMAIL')}}
</b></p>
<p></p>
Thanks & Regards,<br>
The Support team,<br>
<b>{{ config('app.name') }}</b>
@endcomponent