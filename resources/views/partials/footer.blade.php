<!-- Jquery Core Js -->
<script src="{{asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap Core Js -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
<!-- Select Plugin Js -->
<script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Slimscroll Plugin Js -->
<script src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
<!-- Waves Effect Plugin Js -->
<script src="{{asset('plugins/node-waves/waves.js') }}"></script>
<script src="{{asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{asset('js/typeahead.min.js')}}"></script>
<script src="{{asset('js/dropzone.min.js')}}"></script>
<script src="{{asset('js/splide.min.js')}}"></script>
<script src="{{asset('plugins/momentjs/moment.js')}}"></script>
<script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<!-- Custom Js -->
<script src="{{asset('js/signature_pad.min.js') }}"></script>
<script src="{{asset('js/admin.js') }}"></script>
<script src="{{asset('plugins/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<!-- Demo Js -->
<script src="{{asset('js/custom.js') }}"></script>