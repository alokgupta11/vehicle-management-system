<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\UserInterface;
use App\Repositories\Interfaces\OrderInterface;
use Illuminate\Http\Request;
use Validator;
use Session;
use Hash;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderDeliveredCustomer;
use App\Mail\OrderDeliveredAdmin;

class OrderController extends Controller
{
    protected $user;
    protected $order;

    public function __construct(UserInterface $user,OrderInterface $order)
    {
        $this->middleware('driver');
        $this->user = $user;
        $this->order = $order;
    }

    public function listOrders()
    {
        $pageType = 'admin_order_index';
        $orders = $order = $this->order->listOrders();
        return view('order.index',compact('pageType','orders'));
    }

    public function listMyOrders()
    {
        $pageType = 'driver_order_index';
        $user = Auth::user();
        $orders = $this->order->listMyOrders($user);
        return view('order.driver_index',compact('pageType','orders'));
    }

    public function customerSelectSearch(Request $request)
    {
        $customers = [];
        if($request->has('q')){
            $search = $request->q;
            $customers = $this->user->getCustomersSearchList($search);
        }
        return response()->json($customers);
    }

    public function driverSelectSearch(Request $request)
    {
        $drivers = [];
        if($request->has('q')){
            $search = $request->q;
            $drivers = $this->user->getDriversSearchList($search);
        }
        return response()->json($drivers);
    }

    public function createOrder()
    {
        $pageType = 'admin_order_create';
        $order_uuid = unique_random_hash('orders','order_uuid',12);
        return view('order.create',compact('pageType','order_uuid'));
    }

    public function saveOrder(Request $request)
    {
        $data = $request->all();
        $rules = [
            'order_uuid' => 'required|size:12',
            'customer_uuid' => 'required|size:3',
            //'type' => "required",
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        unset($data['_token']);
        $data['status'] = 'Driver UnAssigned';
        $data['created_by_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
        $order = $this->order->createOrder($data);
        
        $log['order_id'] = $order->order_uuid;
        $log['user_id'] = Auth::user()->id;
        $log['action'] = 'CREATE';
        $curr_user = current_user();
        $log['message'] = "$curr_user created the Order #$order->order_uuid";
        activity_logg($log);
        
        Session::flash('success_msg','Order created successfully.');
        return redirect()->to('edit/order/'.$order->order_uuid.'?edit=vehicle');
    }

    public function editOrder(Request $request, $order_uuid)
    {
        $pageType = 'admin_order_edit';
        $order = $this->order->getOrderDetails($order_uuid);
        $vehicle = $this->order->getOrderVehicleDetails($order_uuid);
        if($vehicle)
        {
            $vehicle->images = $this->getVehicleImages($order_uuid,$vehicle->id);
        }
        $pickup_delivery = $this->order->getOrderPickupDeliveryDetails($order_uuid);
        if(isset($request->id))
        {
            $pd_edit = $this->order->getOrderPickupDeliveryDetailById($request->id);
            $pd_type = 'edit';
        }
        else
        {
            $pd_edit = collect();
            $pd_type = 'create';
        }
        if(count($pickup_delivery) == 0)
        {
            $show_confirm = false;
        }
        else
        {
            $show_confirm = true;
            foreach($pickup_delivery as $pd)
            {
                if(is_null($pd->date_time))
                {
                  $show_confirm = false;
                  break;
                }
            }
        }
    
        return view('order.edit',compact('pageType','order','vehicle','pickup_delivery','pd_edit','pd_type','show_confirm'));
    }

    public function updateOrder(Request $request,$order_uuid)
    {
        $data = $request->all();

        if($request->input('action') == 'customer')
        {
            $cust_data = [];
            $rules = [
                'customer_uuid' => 'required|size:3',
                //'type' => "required",
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);
            $cust_data['order_uuid'] = $data['order_uuid'];
            //$cust_data['type'] = $data['type'];
            $cust_data['customer_uuid'] = $data['customer_uuid'];
            $cust_data['updated_by_id'] = Auth::user()->id;
            $cust_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
            $order = $this->order->updateOrderCustomer($cust_data);

            $log['order_id'] = $order_uuid;
            $log['user_id'] = Auth::user()->id;
            $log['action'] = 'UPDATE';
            $curr_user = current_user();
            $log['message'] = "$curr_user updated the customer for Order #$order_uuid";
            activity_logg($log);
            
            Session::flash('success_msg','Order: Customer details updated successfully.');
            return redirect()->back();
        }

        if($request->input('action') == 'vehicle')
        {
            $vehi_data = [];
            $rules = [
                'year' => 'required|not_in:0',
                'condition' => 'required|not_in:0',
                'is_keys' => 'required|not_in:0',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);
            $vehi_data['order_id'] = $data['order_id'];
            $vehi_data['year'] = $data['year'];
            $vehi_data['make'] = $data['make'];
            $vehi_data['model'] = $data['model'];
            $vehi_data['vin'] = $data['vin'];
            $vehi_data['stock'] = $data['stock'];
            $vehi_data['condition'] = $data['condition'];
            $vehi_data['is_keys'] = $data['is_keys'];
            $vehi_data['comment'] = $data['comment'];

            $vehicle_check = $this->order->checkVehicleDataExist($vehi_data['order_id']);

            if(count($vehicle_check) > 0)
            {
                $vehi_data['updated_by_id'] = Auth::user()->id;
                $vehi_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
                $result = $this->order->updateOrderVehicle($vehi_data);
                $log['order_id'] = $order_uuid;
                $log['user_id'] = Auth::user()->id;
                $log['action'] = 'UPDATE';
                $curr_user = current_user();
                $log['message'] = "$curr_user updated the Vehicle detail for Order #$order_uuid";
                activity_logg($log);
            }
            else
            {
                $vehi_data['created_by_id'] = Auth::user()->id;
                $vehi_data['created_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
                $result = $this->order->createOrderVehicle($vehi_data);
                $log['order_id'] = $order_uuid;
                $log['user_id'] = Auth::user()->id;
                $log['action'] = 'CREATE';
                $curr_user = current_user();
                $log['message'] = "$curr_user created the Vehicle detail for Order #$order_uuid";
                activity_logg($log);
            }

            Session::flash('success_msg','Order: Vehicle details updated successfully.');
            return redirect()->back()->withInput($request->input());
        }

        if($request->input('action') == 'pick-drop')
        {
            $pd_data = [];
            $rules = [
                //'driver_uuid' => 'required|size:10',
                'pickup_location' => 'required',
                'pickup_zip' => 'required',
                'pickup_address' => 'required',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);
            $pd_data['order_id'] = $data['order_id'];
            $pd_data['pickup_location'] = $data['pickup_location'];
            $pd_data['pickup_zip'] = $data['pickup_zip'];
            $pd_data['pickup_address'] = $data['pickup_address'];

            $pd_data['delivery_location'] = $data['delivery_location'];
            $pd_data['delivery_zip'] = $data['delivery_zip'];
            $pd_data['delivery_address'] = $data['delivery_address'];

            $pd_data['driver_uuid'] = $data['driver_uuid'];
            $pd_data['pickup_time'] = $data['pickup_time'];
            $pd_data['delivery_time'] = $data['delivery_time'];

            $pd_data['is_final_destination'] = isset($data['is_final_destination']) ? $data['is_final_destination'] : null;

            $order_status = 'Driver UnAssigned';
            
            if(!is_null($data['driver_uuid']))
            {
                $pd_data['status'] = 'Driver Assigned';
                $order_status = 'Driver Assigned';
                $log['order_id'] = $order_uuid;
                $log['user_id'] = Auth::user()->id;
                $log['action'] = 'STATUS';
                $curr_user = current_user();
                $driver_uuidd = $data['driver_uuid'];
                $log['message'] = "Driver #$driver_uuidd assigned to the Order #$order_uuid";
                activity_logg($log);
                if(!is_null($data['pickup_time']))
                {
                    $pd_data['status'] = 'Picked';
                    $order_status = 'In Transit';
                    $log['order_id'] = $order_uuid;
                    $log['user_id'] = Auth::user()->id;
                    $log['action'] = 'STATUS';
                    $curr_user = current_user();
                    $log['message'] = "$curr_user picked the Order #$order_uuid";
                    activity_logg($log);
                }
                if(!is_null($data['delivery_time']))
                {
                    $pd_data['status'] = 'Delivered';
                    $order_status = 'In Transit';
                    $log['order_id'] = $order_uuid;
                    $log['user_id'] = Auth::user()->id;
                    $log['action'] = 'STATUS';
                    $curr_user = current_user();
                    $log['message'] = "$curr_user delivered the Order #$order_uuid";
                    activity_logg($log);
                }
            }
            else
            {
                $pd_data['status'] = 'Driver UnAssigned';
                $order_status = 'Driver UnAssigned';
            }

            $order_data = ['order_uuid' => $order_uuid,
                           'status' => $order_status,
                           'updated_by_id' => Auth::user()->id,
                           'updated_by' => Auth::user()->first_name." ".Auth::user()->last_name,
                           ];
            $this->order->updateOrderStatus($order_data);

            $pd_data['created_by_id'] = Auth::user()->id;
            $pd_data['created_by'] = Auth::user()->first_name." ".Auth::user()->last_name;

            $result = $this->order->createOrderPickupDelivery($pd_data);

            $log['order_id'] = $order_uuid;
            $log['user_id'] = Auth::user()->id;
            $log['action'] = 'CREATE';
            $curr_user = current_user();
            $log['message'] = "$curr_user created the Pickup/delivery Entry for Order #$order_uuid";
            activity_logg($log);

            Session::flash('success_msg','Order: Pickup/Delivery details created successfully.');
            return redirect()->back();
        }

        if($request->input('action') == 'pick-drop-update')
        {
            $pd_data = [];
            $pd_curr = $this->order->getOrderPickupDeliveryById($data['pick_deli_id']);
            $rules = [
                'pickup_location' => 'required',
                'pickup_zip' => 'required',
                'pickup_address' => 'required',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);

            $pd_data['order_id'] = $data['order_id'];
            $pd_data['pickup_location'] = $data['pickup_location'];
            $pd_data['pickup_zip'] = $data['pickup_zip'];
            $pd_data['pickup_address'] = $data['pickup_address'];

            $pd_data['delivery_location'] = $data['delivery_location'];
            $pd_data['delivery_zip'] = $data['delivery_zip'];
            $pd_data['delivery_address'] = $data['delivery_address'];

            $pd_data['driver_uuid'] = $data['driver_uuid'];
            $pd_data['pickup_time'] = $data['pickup_time'];
            $pd_data['delivery_time'] = $data['delivery_time'];

            $pd_data['is_final_destination'] = isset($data['is_final_destination']) ? $data['is_final_destination'] : null;

            $order_status = 'Driver UnAssigned';
            
            if(!is_null($data['driver_uuid']))
            {
                $pd_data['status'] = 'Driver Assigned';
                $order_status = 'Driver Assigned';
                if($pd_curr->driver_uuid != $data['driver_uuid'])
                {
                    $log['order_id'] = $order_uuid;
                    $log['user_id'] = Auth::user()->id;
                    $log['action'] = 'STATUS';
                    $curr_user = current_user();
                    $driver_uuidd = $data['driver_uuid'];
                    $log['message'] = "Driver #$driver_uuidd assigned to the Order #$order_uuid";
                    activity_logg($log); 
                }
                
                if(!is_null($data['pickup_time']))
                {
                    $pd_data['status'] = 'Picked';
                    $order_status = 'In Transit';
                    if($pd_curr->pickup_time != $data['pickup_time'])
                    {
                        $log['order_id'] = $order_uuid;
                        $log['user_id'] = Auth::user()->id;
                        $log['action'] = 'STATUS';
                        $curr_user = current_user();
                        $log['message'] = "$curr_user picked the Order #$order_uuid";
                        activity_logg($log);
                    }
                }
                if(!is_null($data['delivery_time']))
                {
                    $pd_data['status'] = 'Delivered';
                    $order_status = 'In Transit';
                    if($pd_curr->delivery_time != $data['delivery_time'])
                    {
                    $log['order_id'] = $order_uuid;
                    $log['user_id'] = Auth::user()->id;
                    $log['action'] = 'STATUS';
                    $curr_user = current_user();
                    $log['message'] = "$curr_user delivered the Order #$order_uuid";
                    activity_logg($log);
                    }
                }
            }
            else
            {
                $pd_data['status'] = 'Driver UnAssigned';
                $order_status = 'Driver UnAssigned';
            }

            $order_data = ['order_uuid' => $order_uuid,
                           'status' => $order_status,
                           'updated_by_id' => Auth::user()->id,
                           'updated_by' => Auth::user()->first_name." ".Auth::user()->last_name,
                           ];
            $this->order->updateOrderStatus($order_data);

            $pd_data['updated_by_id'] = Auth::user()->id;
            $pd_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
            
            $pd_data['id'] = $data['pick_deli_id'];
            $result = $this->order->updateOrderPickupDelivery($pd_data);

            $log['order_id'] = $order_uuid;
            $log['user_id'] = Auth::user()->id;
            $log['action'] = 'UPDATE';
            $curr_user = current_user();
            $log['message'] = "$curr_user updated the Pickup/delivery Entry for Order #$order_uuid";
            activity_logg($log);

            Session::flash('success_msg','Order: Pickup/Delivery details updated successfully.');
            return redirect()->to('edit/order/'.$order_uuid.'?edit=pick-drop');
        }

        if($request->input('action') == 'confirm')
        {
            $order_data = [];
            $rules = [
                'driver_uuid' => 'required',
                'pickup_location' => 'required',
                'pickup_zip' => 'required',
                'pickup_address' => 'required',
                'pickup_time' => 'required',
                'delivery_location' => 'required',
                'delivery_zip' => 'required',
                'delivery_address' => 'required',
                'delivery_time' => 'required',
                'delivery_person' => 'required',
                'receiver' => 'required',
                'condition' => 'required',
                'signature' => 'required'
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);

            $order_data['order_uuid'] = $order_uuid;
            $order_data['delivery_person'] = $data['delivery_person'];
            $order_data['receiver'] = $data['receiver'];
            $order_data['condition'] = $data['condition'];

            $data_uri = $data['signature'];
            if($data['signature'] != 'signature.png')
            {
                $encoded_image = explode(",", $data_uri)[1];
                $decoded_image = base64_decode($encoded_image);
                $sign_path = $order_uuid."/signature.png";
                $path = public_path().'/vimages/'.$order_uuid;
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                file_put_contents(base_path().'/public/vimages/'.$sign_path, $decoded_image);
                $order_data['signature'] = $sign_path;
            }
            $order_data['status'] = 'Delivered';
            $order_data['updated_by_id'] = Auth::user()->id;
            $order_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
            $result = $this->order->updateOrderCustomer($order_data);

            $order = $this->order->getOrderDetails($order_uuid);
            $user = $this->user->getUserByUUID($order->customer_uuid);

            Mail::to($user->email)->send(new OrderDeliveredCustomer($order,$user));
            Mail::to(env('MAIL_TO_ADDRESS'))->send(new OrderDeliveredAdmin($order,$user));
            
            $log['order_id'] = $order_uuid;
            $log['user_id'] = Auth::user()->id;
            $log['action'] = 'STATUS';
            $curr_user = current_user();
            $log['message'] = "$curr_user delivered the Order #$order_uuid to the final destination";
            activity_logg($log);

            Session::flash('success_msg','Order: Delivered successfully.');
            if(Auth::user()->role_id == '1')
            {
                return redirect()->to('orders');
            }
            elseif(Auth::user()->role_id == '2')
            {
                return redirect()->to('my-orders');
            }
            
        }
        
    }

    public function uploadVehicleImages(Request $request,$order_uuid)
    {
        $order_id = $request->order_id;
        $vehicle_id = $request->vehicle_id;
        if($request->hasFile('file'))
        {
            $image = $request->file('file');
            if ($image->isValid()) {
                $image_name = Str::random(14).".". $image->getClientOriginalExtension();
                $base_path = base_path().'/public/vimages/';
                $db_path = $order_uuid."/".$image_name;
                $path = base_path().'/public/vimages/'.$order_uuid;
                $image->move($path,$image_name);
                $vehi_image = ['vehicle_id' => $vehicle_id,
                               'driver_id' => Auth::user()->id,
                               'image_name' => $image_name,
                              'image_path' => $db_path,
                              'created_by_id' => Auth::user()->id,
                              'created_by' => Auth::user()->first_name." ".Auth::user()->last_name,
                              'updated_by_id' => Auth::user()->id,
                              'updated_by' => Auth::user()->first_name." ".Auth::user()->last_name,
                             ];
                $result = $this->order->createOrderVehicleImage($vehi_image);
                $log['order_id'] = $order_uuid;
                $log['user_id'] = Auth::user()->id;
                $log['action'] = 'CREATE';
                $curr_user = current_user();
                $log['message'] = "$curr_user uploaded image to the order #$order_uuid";
                activity_logg($log);
            }
        }
        
        return $image_name;
    }

    public function getVehicleImages($order_uuid,$vehicle_id)
    {
       if(Auth::user()->role_id == '1')
       {
         $images = $this->order->getVehicleImages($vehicle_id);
       }
       else
       {
         $images = $this->order->getVehicleImagesByDriver($vehicle_id,Auth::user()->id);
       }
       
       return $images;
    }

    public function deleteVehicleImage(Request $request,$order_uuid)
    {
       $file_name = $request->file_name;
       $path = public_path()."/vimages/".$order_uuid."/".$file_name;
       if(File::exists($path))
       {
         File::delete($path);
       }
       $result = $this->order->deleteVehicleImage($order_uuid,$file_name);
        $log['order_id'] = $order_uuid;
        $log['user_id'] = Auth::user()->id;
        $log['action'] = 'DELETE';
        $curr_user = current_user();
        $log['message'] = "$curr_user deleted image from the order #$order_uuid";
       return $result;
    }

    public function deleteOrder($order_uuid)
    {
       $order_id = $this->order->getOrderIdByUuid($order_uuid);
       $vehicle_id = $this->order->getVehicleIdByOrderId($order_id);
       $images = $this->order->getVehicleImages($vehicle_id);
       if(count($images) > 0)
       {
           foreach($images as $image)
           {
               $path = public_path()."/vimages/".$image->image_path;
               if(File::exists($path))
               {
                 File::delete($path);
               }
           }
       }
       $result = $this->order->deleteAllVehicleImagesByVehicleId($vehicle_id);
       $result = $this->order->deleteVehicleDetailByVehicleId($vehicle_id);
       $result = $this->order->deleteAllOrderPickupDelivery($order_uuid);
       $result = $this->order->deleteOrder($order_uuid);

        $log['order_id'] = $order_uuid;
        $log['user_id'] = Auth::user()->id;
        $log['action'] = 'DELETE';
        $curr_user = current_user();
        $log['message'] = "$curr_user deleted the Order #$order_uuid";
        activity_logg($log);
       Session::flash('success_msg','Order details deleted successfully.');
       return redirect()->back();
    }

    public function deleteOrderPickDelivery($order_uuid,$pd_id)
    {
        $result = $this->order->deleteOrderPickDelivery($order_uuid,$pd_id);
        $log['order_id'] = $order_uuid;
        $log['user_id'] = Auth::user()->id;
        $log['action'] = 'DELETE';
        $curr_user = current_user();
        $log['message'] = "$curr_user deleted the Pickup/delivery Entry for Order #$order_uuid";
        activity_logg($log);
        Session::flash('success_msg','Order: Pickup/Delivery entry deleted successfully.');
        return redirect()->back();
    }
}