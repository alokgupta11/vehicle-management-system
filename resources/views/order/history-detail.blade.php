@extends('layouts.app')
@section('title')
Orders History Details
@endsection('title')
@section('content')
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="header">
    <h2>
        Orders History Details
        <a href="{{url('order/history/'.$pickup->order_id.'')}}" class="btn bg-green waves-effect pull-right">
           <b><< Back</b>
        </a> 
    </h2>
</div>

@if(session('success_msg'))
      <div class="alert alert-success alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('success_msg') }}
      </div>
    @endif

            <div class="body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Order Id:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->order_id}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Driver Id:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->driver_uuid}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Is Final Destination:</label>
                            </div>
                            <div class="col-md-9">
                                <b>@if($pickup->is_final_destination == 1) Yes @else No @endif</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Pickup Location:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->pickup_location}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Pickup Address:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->pickup_address}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Pickup Zip :</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->pickup_zip}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Pickup Time:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->pickup_time}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Delivery Location:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->delivery_location}}</b>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Delivery Address:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->delivery_address}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Delivery Zip:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->delivery_zip}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Delivery Time:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->delivery_time}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Status:</label>
                            </div>
                            <div class="col-md-9">
                                @if($pickup->status == 'Driver UnAssigned')
                                <label class="label bg-orange">{{$pickup->status}}</label>
                                @elseif($pickup->status == 'Driver Assigned')
                                <label class="label bg-cyan">{{$pickup->status}}</label>
                                @elseif($pickup->status == 'Picked')
                                <label class="label bg-orange">{{$pickup->status}}</label>
                                @elseif($pickup->status == 'Delivered')
                                <label class="label bg-green">{{$pickup->status}}</label>
                                @elseif($pickup->status == 'Delivered by Driver')
                                <label class="label bg-indigo">{{$pickup->status}}</label>
                                @elseif($pickup->status == 'Delivered- Waiting to Reassign')
                                <label class="label bg-orange">{{$pickup->status}}</label>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Delivery Person:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->delivery_person}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Receiver Name:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->receiver}}</b>
                            </div>
                        </div>
                    </div>  

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Condition:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->ocondition}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Created on:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->created_at}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Updated on:</label>
                            </div>
                            <div class="col-md-9">
                                <b>{{$pickup->updated_at}}</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Signature:</label>
                            </div>
                            <div class="col-md-9">
                                <a href="{{asset('vimages/')}}/{{$pickup->signature}}" target="_blank">
                                    <img src="{{asset('vimages/')}}/{{$pickup->signature}}" style="max-height: 100px">
                                </a>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <h4>Vehicle Images</h4>

                @foreach($images as $image)
                    <div class="col-md-4">
                        <a href="{{asset('vimages/')}}/{{$image->image_path}}" target="_blank">
                                    <img src="{{asset('vimages/')}}/{{$image->image_path}}" style="max-height: 100px">
                        </a>
                    </div>
                @endforeach

                </div>
        </div>
</div>
</div>
</div>
@endsection('content')