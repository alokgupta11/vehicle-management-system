<?php 

namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\UserInterface;
use App\Models\User;
use DB;
use Cache;

class DbUser implements UserInterface
{

	protected $user;

	public function __construct(User $user)
	{
		$this->db_user = $user;
	}

	public function getAllCustomers()
	{
		return $this->db_user->where('role_id','3')->orderBy('updated_at','DESC')->get();
	}

	public function getAllDrivers()
	{
		return $this->db_user->where('role_id','2')->orderBy('updated_at','DESC')->get();
	}

	public function getAllAdmins()
	{
		return $this->db_user->where('role_id','1')->orderBy('updated_at','DESC')->get();
	}

	public function getUserById($id)
	{
		return $this->db_user->where('id',$id)->first();
	}

	public function getUserIDByUUID($uuid)
	{
		return $this->db_user->where('user_uuid',$uuid)->value('id');
	}

	public function getUserByUUID($uuid)
	{
		return $this->db_user->where('user_uuid',$uuid)->first();
	}

	public function createUser($data)
	{
		return $this->db_user->create($data);
	}

	public function updateUser($uuid,$data)
	{
		return $this->db_user->where('user_uuid',$uuid)->update($data);
	}

	public function deleteUserByUUID($uuid)
	{
		return $this->db_user->where('user_uuid',$uuid)->delete();
	}

	public function deleteUserById($id)
	{
		return $this->db_user->where('id',$id)->delete();
	}

	public function updatePassword($id,$data)
	{
        return $this->db_user->where('id',$id)->update($data);
	}

	public function getCustomersSearchList($search)
    {
        $customers = $this->db_user->where('role_id','=','3')
								   ->where(function ($q) use($search) {
								     $q->orWhere('first_name', 'LIKE', "%$search%")
		                             ->orWhere('last_name', 'LIKE', "%$search%")
		                             ->orWhere('user_uuid', 'LIKE', "%$search%");
								})->get(["id","user_uuid","first_name","last_name",'email']);
	                             
	                             
        return $customers;
    }

    public function getDriversSearchList($search)
    {
        $customers = $this->db_user->where('role_id','=','2')
								   ->where(function ($q) use($search) {
								     $q->orWhere('first_name', 'LIKE', "%$search%")
		                             ->orWhere('last_name', 'LIKE', "%$search%")
		                             ->orWhere('user_uuid', 'LIKE', "%$search%");
								})->get(["id","user_uuid","first_name","last_name",'email']);
	                             
	                             
        return $customers;
    }

}