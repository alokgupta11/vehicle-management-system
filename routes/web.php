<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__.'/auth.php';

Route::get('test', 'TestController@hello');
Route::get('logout', 'UserController@logout');

//User routes
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home_page');
    Route::get('dashboard', 'HomeController@index')->name('user_dash');
    Route::get('profile', 'UserController@account')->name('profile_edit');
    Route::post('profile', 'UserController@updateAccount')->name('profile_update');
    Route::get('edit-password', 'UserController@changePassword')->name('password_edit');
    Route::post('edit-password', 'UserController@updatePassword')->name('password_update');
});

//Admin Routes
Route::group(['middleware' => 'admin'], function () {

    Route::get('admin', 'Admin\UserController@index')->name('admin_dash');
    
    Route::get('customers', 'Admin\UserController@listCustomers')->name('customer_index');
    Route::get('create/customer', 'Admin\UserController@createCustomer')->name('customer_create');
    Route::post('create/customer', 'Admin\UserController@saveCustomer')->name('customer_save');
    Route::get('edit/customer/{uuid}', 'Admin\UserController@editCustomer')->name('customer_edit');
    Route::post('edit/customer/{uuid}', 'Admin\UserController@updateCustomer')->name('customer_update');    
    Route::get('delete/customer/{uuid}', 'Admin\UserController@deleteCustomer')->name('customer_delete');

    Route::get('drivers', 'Admin\UserController@listDrivers')->name('driver_index');
    Route::get('create/driver', 'Admin\UserController@createDriver')->name('driver_create');
    Route::post('create/driver', 'Admin\UserController@saveDriver')->name('driver_save');
    Route::get('edit/driver/{uuid}', 'Admin\UserController@editDriver')->name('driver_edit');
    Route::post('edit/driver/{uuid}', 'Admin\UserController@updateDriver')->name('driver_update');
    Route::get('delete/driver/{uuid}', 'Admin\UserController@deleteDriver')->name('driver_delete');

    Route::get('admins', 'Admin\UserController@listAdmins')->name('admin_index');
    Route::get('create/admin', 'Admin\UserController@createAdmin')->name('admin_create');
    Route::post('create/admin', 'Admin\UserController@saveAdmin')->name('admin_save');
    Route::get('edit/admin/{uuid}', 'Admin\UserController@editAdmin')->name('admin_edit');
    Route::post('edit/admin/{uuid}', 'Admin\UserController@updateAdmin')->name('admin_update');
    Route::get('delete/admin/{uuid}', 'Admin\UserController@deleteAdmin')->name('admin_delete');
    
    Route::get('orders', 'Admin\OrderController@listOrders')->name('admin_order_index');
    Route::get('delete/order/{order_uuid}', 'Admin\OrderController@deleteOrder')->name('admin_order_delete');

    Route::get('confirm_order/order/{order_uuid}', 'Admin\OrderController@confirm_delivery')->name('admin_order_confirm');

});

Route::group(['middleware' => 'driver'], function () {

	Route::get('my-orders', 'Admin\OrderController@listMyOrders')->name('driver_order_index');
    
    Route::get('create/order', 'Admin\OrderController@createOrder')->name('admin_order_create');
    Route::post('create/order', 'Admin\OrderController@saveOrder')->name('admin_order_save');
    Route::post('order/update', 'Admin\OrderController@updateOrderDetails')->name('admin_order_update_details');


    Route::get('order/package/{order_uuid}', 'Admin\OrderController@assignDriver')->name('admin_order_edit');
    Route::get('order/reassign/{order_uuid}', 'Admin\OrderController@reAssignDriver')->name('admin_order_edit');
    Route::post('order/package/{order_uuid}', 'Admin\OrderController@updateOrder2')->name('admin_order_update2');
    Route::get('order/history/{order_uuid}', 'Admin\OrderController@orderHitoryById')->name('order_history_index');

    Route::get('order/history_detail/{id}', 'Admin\OrderController@orderHistoryDetail')->name('order_history_detail');

     Route::post('destination-search', 'Admin\OrderController@SearchDelivery')->name('destination_search');
    /*************************** UPDATE FROM DRIVER PANEL FOR SHIPMENT *****************************/

    Route::get('update/shipment/{id}/{order_uuid}', 'Admin\OrderController@editShipment')->name('admin_order_edit'); 
    Route::post('update/shipment/{order_uuid}', 'Admin\OrderController@updateShipment')->name('admin_update_shipment');

    /*************************** UPDATE FROM DRIVER PANEL FOR SHIPMENT *****************************/

    Route::get('edit/order/{order_uuid}', 'Admin\OrderController@editOrder')->name('admin_order_edit');
    Route::post('edit/order/{order_uuid}', 'Admin\OrderController@updateOrder')->name('admin_order_update');

    Route::post('upload/vehicle-images/{order_uuid}', 'Admin\OrderController@uploadVehicleImages')->name('admin_vehicle_image_upload');

    Route::post('delete/vehicle-image/{order_uuid}', 'Admin\OrderController@deleteVehicleImage')->name('admin_delete_vehicle_image');
    Route::get('get/vehicle-images/{order_uuid}/{vehicle_id}', 'Admin\OrderController@getVehicleImages')->name('admin_get_vehicle_image');
    Route::get('customer-search', 'Admin\OrderController@customerSelectSearch')->name('customer_search');
    Route::get('driver-search', 'Admin\OrderController@driverSelectSearch')->name('driver_search');
    Route::get('delete/pick-drop/{order_uuid}/{pd_id}', 'Admin\OrderController@deleteOrderPickDelivery')->name('admin_order_pd_delete');
});