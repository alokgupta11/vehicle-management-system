<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Interfaces\OrderInterface;

class HomeController extends Controller
{
    protected $order;
    public function __construct(OrderInterface $order)
    {
        $this->order = $order;
    }

    public function index()
    {
        if(Auth::check())
        {
            if (Auth::user()->role_id == 1) {
                return redirect()->to('admin');
            }
            elseif(Auth::user()->role_id == 2)
            {
                $pageType = 'user_dash';
                $user = Auth::user();
                $stats = $this->order->getDriverStats($user);
                return view('dashboard',compact('pageType','stats'));
            }
            elseif(Auth::user()->role_id == 3)
            {
                Auth::logout();
                return redirect()->to('login');
            }
        }
        else
        {
            return redirect()->to('login');
        }
    }
}
