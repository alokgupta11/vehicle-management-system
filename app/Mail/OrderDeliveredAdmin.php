<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Order;

class OrderDeliveredAdmin extends Mailable
{
    use Queueable, SerializesModels;

     public $order;
     public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order,User $user)
    {
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.order_delivered_admin')
                    ->subject('Order - Delivered successfully');
    }
}
