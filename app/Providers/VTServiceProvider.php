<?php 

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class VTServiceProvider extends ServiceProvider {

    public function register(){
    	$this->app->bind(
            'App\Repositories\Interfaces\UserInterface',
            'App\Repositories\Eloquents\DbUser'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\OrderInterface',
            'App\Repositories\Eloquents\DbOrder'
        );
    }
}