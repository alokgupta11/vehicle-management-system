@extends('layouts.app')
@section('title')
Orders List
@endsection('title')
@section('content')
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="header">
    <h2>
        <a href="{{url('orders')}}" class="btn bg-indigo waves-effect pull-right">
           <b> Orders List</b>
        </a>
        <h2>Assign Driver to Order ID - {{$order->order_uuid}}</h2>
    </h2>
</div>
<div class="body">

    <br>
    @if(session('success_msg'))
      <div class="alert alert-success alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('success_msg') }}
      </div>
    @endif
    @if(session('error_msg'))
      <div class="alert alert-danger alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('error_msg') }}
      </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
                
    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active">
         
            <form class="form_validation" method="POST" action="{{route('admin_order_update2',$order->order_uuid)}}">
            @csrf
            <input type="hidden" name="order_id" value="{{$order->id}}">
             <input type="hidden" name="order_uuid" value="{{$order->order_uuid}}" id="order_uuid">
            @if($pd_type == 'edit')
            <input type="text" name="pick_deli_id" value="{{$pd_edit->id}}">
            @endif
            <input type="hidden" name="first_driver" value="{{$order->first_driver}}">
            <!-- <h5>Enter Pickup/Delivery Details</h5> -->
           <h5>Select Driver:</h5>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        @if(Auth::user()->role_id == '1')
                        <input type="text" class="form-control livesearchDriver" name="driver_uuid" required value="{{isset($pd_edit->driver_uuid) ? $pd_edit->driver_uuid : old('driver_uuid')}}">
                        @elseif(Auth::user()->role_id == '2')
                        <input type="text" class="form-control" name="driver_uuid" readonly value="{{Auth::user()->user_uuid}}">
                        @endif
                        <label class="form-label">Enter Driver ID</label>
                    </div>
                </div>
                </div>
            </div>


            <h3>Enter PICKUP & DELIVERY details:</h3>
            <hr>
             <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-primary">
                      <div class="panel-heading" style="text-align: center;font-size: 20px;">Pickup Details</div>
                      <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">

                                    
                                        <input type="text" class="form-control" name="pickup_location" value="{{isset($last_delivery_destination->delivery_location) ? $last_delivery_destination->delivery_location : old('pickup_location')}}" id="dd">
                                    
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">

                                        <input type="text" class="form-control" name="pickup_zip" value="{{isset($last_delivery_destination->delivery_zip) ? $last_delivery_destination->delivery_zip : old('pickup_zip')}}">
                                    
                                    <label class="form-label">Pickup Zip Code</label>
                                </div>
                            </div>
                            </div>

                           <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">

                                        <textarea class="form-control" name="pickup_address">{{isset($last_delivery_destination->delivery_address) ? $last_delivery_destination->delivery_address : old('pickup_address')}} </textarea>

                                   
                                    <label class="form-label">Pickup Address</label>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control datetimepicker" name="pickup_time" value="{{isset($pd_edit->pickup_time) ? $pd_edit->pickup_time : old('pickup_time')}}">
                                        <label class="form-label">Pickup Time</label>
                                    </div>
                                </div>
                            </div>  

                        </div>

                      </div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="panel panel-success" style="    border-color: #008000;">
                      <div class="panel-heading" style="text-align: center;font-size: 20px;background-color: green;color: white">Delivery Details</div>
                      <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line res">
                                    <input type="text" class="form-control" id="delivery_location" name="delivery_location" value="{{isset($pd_edit->delivery_location) ? $pd_edit->delivery_location : old('delivery_location')}}">
                                    <label class="form-label">Delivery Location</label>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line res">
                                     <input type="text" class="form-control" id="delivery_zip" name="delivery_zip" value="{{isset($pd_edit->delivery_zip) ? $pd_edit->delivery_zip : old('delivery_zip')}}">
                                     <label class="form-label">Delivery Zipcode</label>
                                </div>
                            </div>
                            </div>

                           <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line res">
                                    <textarea class="form-control"id="delivery_address"  name="delivery_address">{{isset($pd_edit->delivery_address) ? $pd_edit->delivery_address : old('delivery_address')}}</textarea>
                                    <label class="form-label">Delivery Address</label>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                       <input type="text" class="form-control datetimepicker" name="delivery_time" value="{{isset($pd_edit->delivery_time) ? $pd_edit->delivery_time : old('delivery_time')}}">
                                         <label class="form-label">Delivery Time</label>
                                    </div>
                                </div>
                            </div>  

                        </div>

                      </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group form-float">
                        <div class="form-line">

                             <label class="form-label">Is Final Destination</label>
                           <select class="form-control" name="is_final_destination" id="is_final_destination" onchange="getDeliveryAddress()">
                               <option value="">Select Is Final Destination</option>
                               <option value="1" {{ isset($pd_edit->is_final_destination) && $pd_edit->is_final_destination == 1 ? 'selected':'' }}>Yes</option>
                               <option value="0" {{ isset($pd_edit->is_final_destination) && $pd_edit->is_final_destination == 0 ? 'selected':'' }}>No</option>
                           </select>
                        </div>
                    </div>
                </div>

            </div>

            
         

            
      
        <center><button class="btn btn-warning waves-effect" type="submit" name="action" value="add" style="font-size: 20px">Re-Assign Order to Driver</button></center>

            <!-- <a class="btn btn-info waves-effect" href="{{url()->current().'?edit=vehicle'}}">Prev</a> -->
            <!-- <p class="deliverBtnWrap {{isset($pd_edit->is_final_destination) ? 'show':'hide'}} {{old('is_final_destination') ? 'show' : 'hide'}}"> -->
                <br>
          <!-- <center>  OR</center>
            <br>
           <center> <button class="btn btn-success waves-effect deliverBtn" type="submit" name="action" value="confirm">Mark Delivered</button></center> -->
            </p>
            </form>
            <hr>

        <!-------------- IF user is driver then enable the upload image option ------------------->
         @if(Auth::user()->role_id == 2)
            <div class="imageUpload">
                <h5>Upload Vehicle Images</h5>
                <br>
                <p>
                    @if(isset($vehicle))
                    <form id="frmFileUpload" class="form_validation dropzone" method="POST" action="{{route('admin_vehicle_image_upload',$order->order_uuid)}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input type="hidden" name="vehicle_id" value="{{$vehicle->id}}">
                    <div class="dz-message">
                        <div class="drag-icon-cph">
                            <i class="material-icons">touch_app</i>
                        </div>
                        <h3>Drop files here or click to upload.</h3>
                    </div>
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                    </form>
                    @else
                    <div class="alert alert-warning alert-dismissible" role="alert">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      Please fill vehicle details to start upload the images!
                    </div>
                    @endif
                </p>
            </div>
        @endif

            </p>
        </div>
    </div>
    </form>
</div>
</div>
</div>
</div>
@endsection



<script type="text/javascript">
    function getDeliveryAddress() {
        var is_final_destination=$("#is_final_destination").val();
        var order_uuid=$("#order_uuid").val();
        if(is_final_destination == 1)
                $.ajax({
                    type: "POST",
                    url: "{{route('destination_search',$order->order_uuid)}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "order_uuid": order_uuid
                    },
                    dataType: 'json',
                    success: function(response){
                          $("#delivery_location").val("Glenn Quagmire");

                        $("#delivery_location").val(response[0].to_location);
                        $("#delivery_address").val(response[0].to_address);
                        $("#delivery_zip").val(response[0].to_zipcode);
                        $(".res").addClass("focused");
                        console.log(response[0].to_location);
                    }
                });
            
    }
</script>