@extends('layouts.app')
@section('title')
Dashboard
@endsection('title')
@section('content')
<h4 class="">Dashboard</h4>
<br>
<div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-blue hover-expand-effect">
            <div class="icon">
                <i class="material-icons">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text">Orders Total</div>
                <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20">{{$stats['order_total']}}</div>
            </div>
        </div>
    </div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-light-green hover-expand-effect">
            <div class="icon">
                <i class="material-icons">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text">Orders Processed</div>
                <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20">{{$stats['order_delivered']}}</div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-orange hover-expand-effect">
            <div class="icon">
                <i class="material-icons">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text">Orders Pending</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">{{$stats['order_pending']}}</div>
            </div>
        </div>
    </div>
</div>
@endsection('content')