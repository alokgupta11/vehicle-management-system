<?php

namespace App\Models;
use Eloquent;

class VehicleImage extends Eloquent {
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'vehicle_images';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id','created_at','updated_at'
    ];
}