@extends('layouts.app')
@section('title')
Update Order Status
@endsection('title')
@section('content')
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="header">
    <h2>
        <a href="{{url('orders')}}" class="btn bg-indigo waves-effect pull-right">
           <b> Orders List</b>
        </a>
        <h2>Update Order Details: Order ID - {{$order->order_uuid}}</h2>
    </h2>
</div>
<div class="body">

    <br>
    @if(session('success_msg'))
      <div class="alert alert-success alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('success_msg') }}
      </div>
    @endif
    @if(session('error_msg'))
      <div class="alert alert-danger alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('error_msg') }}
      </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
                
    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active">
         
            <form class="form_validation" method="POST" action="{{route('admin_update_shipment',$order->order_uuid)}}">
            @csrf
            <input type="hidden" name="order_id" value="{{$order->id}}">
             <input type="hidden" name="order_uuid" value="{{$order->order_uuid}}">

             <input type="hidden" name="pickup_delivery_id" value="{{$pd_edit->id}}">
             <input type="hidden" name="is_final_destination" value="{{$pd_edit->is_final_destination}}">
           

            <!-- <h5>Enter Pickup/Delivery Details</h5> -->
           <h5>Driver:</h5>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        @if(Auth::user()->role_id == '1')
                        <input type="text" class="form-control livesearchDriver" name="driver_uuid" required value="{{isset($pd_edit->driver_uuid) ? $pd_edit->driver_uuid : old('driver_uuid')}}">
                        @elseif(Auth::user()->role_id == '2')
                        <input type="text" class="form-control" name="driver_uuid" readonly value="{{Auth::user()->user_uuid}}">
                        @endif
                        <label class="form-label">Driver ID</label>
                    </div>
                </div>
                </div>
            </div>

        <!--------------------------------------  IF CTION IS UPDATE ------------------------------------- -->

        @if(request()->act == "update")

            <h3>Enter PICKUP & DELIVERY details:</h3>
            <hr>
             <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-primary">
                      <div class="panel-heading" style="text-align: center;font-size: 20px;">Pickup Details</div>
                      <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="pickup_location" value="@if($order->first_driver == '') {{isset($order->from_location) ? $order->from_location : old('from_location')}}   @else {{isset($pd_edit->pickup_location) ? $pd_edit->pickup_location : old('pickup_location')}} @endif">
                                    <label class="form-label">Pickup Location</label>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="pickup_zip" value="@if($order->first_driver == '') {{isset($order->from_zipcode) ? $order->from_zipcode : old('from_zipcode')}}  @else {{isset($pd_edit->pickup_zip) ? $pd_edit->pickup_zip : old('pickup_zip')}} @endif">
                                    <label class="form-label">Pickup Zip Code</label>
                                </div>
                            </div>
                            </div>

                           <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <textarea class="form-control" required name="pickup_address">@if($order->first_driver == '') {{isset($order->from_address) ? $order->from_address : old('from_address')}}  @else {{isset($pd_edit->pickup_address) ? $pd_edit->pickup_address : old('pickup_address')}} @endif </textarea>
                                    <label class="form-label">Pickup Address</label>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control datetimepicker" required name="pickup_time" value="{{isset($pd_edit->pickup_time) ? $pd_edit->pickup_time : old('pickup_time')}}">
                                        <label class="form-label" style="    top: -10px;">Pickup Time</label>
                                    </div>
                                </div>
                            </div>  

                        </div>

                      </div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="panel panel-success" style="    border-color: #008000;">
                      <div class="panel-heading" style="text-align: center;font-size: 20px;background-color: green;color: white">Delivery Details</div>
                      <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" required name="delivery_location" value="{{isset($pd_edit->delivery_location) ? $pd_edit->delivery_location : old('delivery_location')}}">
                                    <label class="form-label">Delivery Location</label>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <input type="text" class="form-control" required name="delivery_zip" required value="{{isset($pd_edit->delivery_zip) ? $pd_edit->delivery_zip : old('delivery_zip')}}">
                                     <label class="form-label">Delivery Zipcode</label>
                                </div>
                            </div>
                            </div>

                           <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <textarea class="form-control" required name="delivery_address">{{isset($pd_edit->delivery_address) ? $pd_edit->delivery_address : old('delivery_address')}}</textarea>
                                    <label class="form-label">Delivery Address</label>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <div class="form-line  ">
                                       <input type="text" class="form-control datetimepicker" required name="delivery_time" value="{{isset($pd_edit->delivery_time) ? $pd_edit->delivery_time : old('delivery_time')}}">
                                         <label class="form-label" style="    top: -10px;">Delivery Time</label>
                                    </div>
                                </div>
                            </div>  

                        </div>

                      </div>
                    </div>
                </div>

            </div>

            <div>
                    <!-- <input type="checkbox" id="basic_checkbox_2" class="filled-in" name="is_final_destination" {{isset($pd_edit->is_final_destination) ? 'checked':''}} {{old('is_final_destination') ? 'checked' : ''}}>  -->
                    <!-- <label for="basic_checkbox_2"><b>Is Final Destination</b></label> -->
                </div>

        @endif
          


        <!-------------------------------------- IF ORDER IS CONFIRMED ---------------------------------->
        @if(request()->act == "confirm-order")
            <div class="">

                 <input type="hidden" name="order_id" value="{{$order->id}}">
                 <input type="hidden" name="order_uuid" value="{{$order->order_uuid}}">

                 <input type="hidden" name="pickup_delivery_id" value="{{$pd_edit->id}}">

                 <input type="hidden" name="pickup_location" value="{{$pd_edit->pickup_location}}">
                 <input type="hidden" name="pickup_address" value="{{$pd_edit->pickup_address}}">
                 <input type="hidden" name="pickup_zip" value="{{$pd_edit->pickup_zip}}">
                 <input type="hidden" name="pickup_time" value="{{$pd_edit->pickup_time}}">

                 <input type="hidden" name="delivery_location" value="{{$pd_edit->delivery_location}}">
                 <input type="hidden" name="delivery_address" value="{{$pd_edit->delivery_address}}">
                 <input type="hidden" name="delivery_zip" value="{{$pd_edit->delivery_zip}}">
                 <input type="hidden" name="delivery_time" value="{{$pd_edit->delivery_time}}">

                 <input type="hidden" name="is_final_destination" value="{{$pd_edit->is_final_destination}}">


                <div class="confirmBlock show">
                   <h5>Enter Delivery confirmation Details</h5>
                    <p>
                       <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="delivery_person" required value="">
                                    <label class="form-label">Delivery person/Driver</label>
                                </div>
                            </div>
                            </div>
                      </div> 
                      <div class="row">
                           <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="receiver" required value="">
                                    <label class="form-label">Receiver name</label>
                                </div>
                            </div>
                           </div>
                           <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="condition" required value="">
                                    <label class="form-label">Condition</label>
                                </div>
                            </div>
                           </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                        <div class="">
                            <div class="">
                                <label class="form-label">Signature</label>
                                <canvas id="signCanvas" width=420 height=200>
                                    
                                </canvas>
                                <!-- <button id="save">Save</button> -->
                            </div>
                            <br>
                            <div class="text-center">
                                <a class="btn btn-danger" id="clear">Clear</a>
                            </div>
                            <div>
                                @if(isset($order->signature))
                                 <img src="{{url('vimages/'.$order->signature)}}">
                                 <input type="hidden" id="signData" name="signature" value="signature.png">
                                @else
                                 <input type="hidden" id="signData" name="signature">
                                @endif
                            </div>
                            
                        </div>
                        </div>
                      </div>

                    </p>
                </div>
            </div>
         
         @endif

            
      @if(request()->act == "update")
        <center><button class="btn btn-warning waves-effect" type="submit" name="action" value="update" style="font-size: 20px">Update Pickup/Delivery Details</button></center>

    @elseif(request()->act == "confirm-order")
        <center> <button class="btn btn-success waves-effect deliverBtn" type="submit" name="action" style="font-size: 20px" value="confirm">Mark Delivered</button></center>
    @endif

            <!-- <a class="btn btn-info waves-effect" href="{{url()->current().'?edit=vehicle'}}">Prev</a> -->
            <!-- <p class="deliverBtnWrap {{isset($pd_edit->is_final_destination) ? 'show':'hide'}} {{old('is_final_destination') ? 'show' : 'hide'}}"> -->
                <br>
          <!-- <center>  OR</center>
            <br>
           <center> <button class="btn btn-success waves-effect deliverBtn" type="submit" name="action" value="confirm">Mark Delivered</button></center> -->
            </p>
            </form>
            <hr>

        <!-------------- IF user is driver then enable the upload image option ------------------->
         @if(Auth::user()->role_id == 2 && request()->act == "update" )
            <div class="imageUpload">
                <h5>Upload Vehicle Images</h5>
                <br>
                <p>
                    @if(isset($vehicle))
                    <form id="frmFileUpload" class="form_validation dropzone" method="POST" action="{{route('admin_vehicle_image_upload',$order->order_uuid)}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input type="hidden" name="vehicle_id" value="{{$pd_edit->id}}">
                    <input type="hidden" name="order_uuid" value="{{$order->order_uuid}}">
                    <div class="dz-message">
                        <div class="drag-icon-cph">
                            <i class="material-icons">touch_app</i>
                        </div>
                        <h3>Drop files here or click to upload.</h3>
                    </div>
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                    </form>
                    @else
                    <div class="alert alert-warning alert-dismissible" role="alert">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      Please fill vehicle details to start upload the images!
                    </div>
                    @endif
                </p>
            </div>
        @endif

            </p>
        </div>
    </div>
    </form>
</div>
</div>
</div>
</div>
@endsection