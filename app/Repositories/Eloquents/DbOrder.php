<?php 

namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\OrderInterface;
use App\Models\User;
use App\Models\Order;
use App\Models\VehicleDetail;
use App\Models\VehicleImage;
use App\Models\PickupDelivery;
use App\Models\OrderHistory;
use DB;
use Cache;

class DbOrder implements OrderInterface
{

	protected $user;
	protected $order;
	protected $vehicle_detail;
	protected $vehicle_image;
	protected $pickup_delivery;
	protected $order_history;

	public function __construct(User $user,Order $order,VehicleDetail $vehicle_detail,VehicleImage $vehicle_image,PickupDelivery $pickup_delivery,OrderHistory $order_history)
	{
		$this->db_user = $user;
		$this->db_order = $order;
		$this->db_vehicle_detail = $vehicle_detail;
		$this->db_vehicle_image = $vehicle_image;
		$this->db_pickup_delivery = $pickup_delivery;
		$this->db_order_history = $order_history;
	}

	public function createOrder($order)
	{
		return $this->db_order->create($order);
	}

	public function listOrders()
	{
		$orders = $this->db_order->join('users','orders.customer_uuid','=','users.user_uuid')
		                         ->orderBy('created_at','asc')
		                         ->get(['orders.*','users.first_name','users.last_name']);
		return $orders;
	}

	public function listMyOrders($user)
	{
		$order_ids = $this->db_pickup_delivery->where('driver_uuid',$user->user_uuid)->distinct()->pluck('order_id');

		$orders = $this->db_order->join('users','orders.customer_uuid','=','users.user_uuid')
		                         ->orderBy('created_at','asc')
		                         ->where('orders.created_by_id',$user->id)
		                         ->orWherein('orders.id',$order_ids)
		                         ->get(['orders.*','users.first_name','users.last_name']);
		return $orders;
	}


	public function listMyOrdersCopy($user)
	{
		$order = $this->db_pickup_delivery->where('driver_uuid',$user->user_uuid)->orderBy("id","desc")->get();
		return $order;
	}


	public function getOrderDetails($order_uuid)
	{
		$order = $this->db_order->where('order_uuid',$order_uuid)->first();
		return $order;
	}


	public function getPickUpDetailsById($id)
	{
		$order = $this->db_pickup_delivery->where('id',$id)->first();
		return $order;
	}


	public function updateOrderCustomer($order)
	{
		$result = $this->db_order->where('order_uuid',$order['order_uuid'])->update($order);
		return $result;
	}

	public function updateVehicleDetails($order)
	{
		$result = $this->db_vehicle_detail->where('order_id',$order['order_id'])->update($order);
		return $result;
	}



	public function getOrderIdByUuid($order_uuid)
	{
		return $this->db_order->where('order_uuid',$order_uuid)->value('id');
	}

	public function getOrderVehicleDetails($order_uuid)
	{
		$order_id = $this->getOrderIdByUuid($order_uuid);
		//return $order_id;
		$vehicle_detail = $this->db_vehicle_detail->where('order_id','=',$order_uuid)->first();

		// $vehicle_detail = $this->db_vehicle_detail->where('order_id','=',$order_id)->first();

		return $vehicle_detail;
	}

	public function getOrderPickupDeliveryDetails($order_uuid)
	{
		$order_id = $this->getOrderIdByUuid($order_uuid);
		$pds = $this->db_pickup_delivery->where('order_id',$order_id)
		                                ->orderBy('created_at','desc')
		                                ->get();
		return $pds;
	}

	public function getOrderPickupDeliveryDetailById($id)
	{
		return $this->db_pickup_delivery->where('id',$id)->first();
	}

	public function checkVehicleDataExist($order_id)
	{
		return $this->db_vehicle_detail->where('order_id','=',$order_id)->get();
	}


	public function checkVehicleImageDataExist($order_uuid,$driver_uuid)
	{
		return $this->db_vehicle_image->where('order_uuid','=',$order_uuid)->where('driver_uuid','=',$driver_uuid)->count();
	}


	public function vimages($id)
	{
		return $this->db_vehicle_image->where('vehicle_id','=',$id)->get();
	}


	public function createOrderVehicle($data)
	{
		return $this->db_vehicle_detail->create($data);
	}

	public function updateOrderVehicle($data)
	{
		return $this->db_vehicle_detail->where('order_id','=',$data['order_id'])->update($data);
	}

	public function createOrderVehicleImage($data)
	{
		return $this->db_vehicle_image->create($data);
	}

	public function getVehicleIdByOrderId($order_id)
	{
		return $this->db_vehicle_detail->where('order_id',$order_id)->value('id');
	}

	public function getVehicleImages($vehicle_id)
	{
		return $this->db_vehicle_image->where('vehicle_id','=',$vehicle_id)->get();
	}

	public function getVehicleImagesByDriver($vehicle_id,$driver_id)
	{
		return $this->db_vehicle_image->where('vehicle_id','=',$vehicle_id)->where('driver_id','=',$driver_id)->get();
	}

	public function deleteVehicleImage($order_uuid,$file_name)
	{
		return $this->db_vehicle_image->where('image_name','=',$file_name)->delete();
	}

	public function deleteAllVehicleImagesByVehicleId($vehicle_id)
	{
		return $this->db_vehicle_image->where('vehicle_id','=',$vehicle_id)->delete();
	}

	public function deleteVehicleDetailByVehicleId($vehicle_id)
	{
		return $this->db_vehicle_detail->where('id','=',$vehicle_id)->delete();
	}

	public function deleteOrder($order_uuid)
	{
		return $this->db_order->where('order_uuid',$order_uuid)->delete();
	}

	public function createOrderPickupDelivery($pd_data)
	{
		return $this->db_pickup_delivery->create($pd_data);
	}

	public function getOrderPickupDeliveryById($pd_id)
	{
		return $this->db_pickup_delivery->where('id',$pd_id)->first();
	}

	public function updateOrderStatus($order_data)
	{
		return $this->db_order->where('order_uuid',$order_data['order_uuid'])->update($order_data);
	}

	// public function updateShipmentOrderStatus($order_data)
	// {
	// 	return $this->db_order->where('id',$order_data['id'])->update($order_data);
	// }

	public function updateOrderPickupDelivery($pd_data)
	{
		return $this->db_pickup_delivery->where('id',$pd_data['id'])->update($pd_data);
	}

	public function deleteOrderPickDelivery($order_uuid,$pd_id)
	{
		$this->db_pickup_delivery->where('id',$pd_id)->delete();
		$pds = $this->getOrderPickupDeliveryDetails($order_uuid);
        
        if(count($pds) == 0)
        {
           $data = ['status' => 'Driver UnAssigned','order_uuid' => $order_uuid];
           $this->updateOrderStatus($data);
        }
        return true;
	}

	public function deleteAllOrderPickupDelivery($order_uuid)
	{
		// 		$order_id = $this->db_order->where('order_uuid',$order_uuid)->value('id');
		return $this->db_pickup_delivery->where('order_id',$order_uuid)->delete();
	}

	public function getAdminStats()
	{
		$stats = [];
		$stats['order_total'] = $this->db_order->count();
		$stats['order_delivered'] = $this->db_order->where('status','Delivered')->count();
		$stats['order_pending'] = $this->db_order->where('status','!=','Delivered')->count();
		$stats['customer_count'] = $this->db_user->where('role_id',3)->count();
		$stats['driver_count'] = $this->db_user->where('role_id',2)->count();
        return $stats;
	}

	public function getDriverStats($user)
	{
		$stats = [];
		
		$stats['order_total'] = $this->db_pickup_delivery->where('driver_uuid',$user->user_uuid)->count();
		$stats['order_delivered'] = $this->db_pickup_delivery->where('driver_uuid',$user->user_uuid)->whereNotNull('delivery_time')->count();
		$stats['order_pending'] = $this->db_pickup_delivery->where('driver_uuid',$user->user_uuid)
		                                                   ->whereNull('delivery_time')
		                                                   ->count();
        return $stats;
	}

	public function getOrderHistory()
	{
        return $this->db_order_history
                    ->orderBy('id','desc')
                    ->take(1000)
                    ->get();
	}


	public function getOrderDetailsByOrderId($order_uuid=null,$driver_uuid=null)
	{
		if($driver_uuid == ''){
			$order = $this->db_pickup_delivery->where('order_id',$order_uuid)->get();
		} else {
			$order = $this->db_pickup_delivery->where('order_id',$order_uuid)->where('driver_uuid',$driver_uuid)->get();
		}
		
		return $order;

	}


	public function getlastAddress($order_uuid)
	{
		
		$sql = $this->db_pickup_delivery->where('order_id',$order_uuid)->orderByDesc('id')->limit(1)->first();
		
		
		return $sql;

	}


	public function getlastPickupDeliveryAddress($order_uuid)
	{
		
		$sql = $this->db_pickup_delivery->where('order_id',$order_uuid)->orderByDesc('id')->limit(1)->first();
		
		
		return $sql;

	}


}