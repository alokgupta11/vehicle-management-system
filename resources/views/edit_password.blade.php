@extends('layouts.app')
@section('title')
Edit Password
@endsection('title')
@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Edit Password</h2>
            </div>
            <div class="body">
            	@if(session('success_msg'))
		          <div class="alert alert-success alert-dismissible" role="alert">
		             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		              {{ session('success_msg') }}
		          </div>
			    @endif
            	@if ($errors->any())
			    <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif
                <form id="form_validation" method="POST" action="{{route('password_update')}}">
                	@csrf
                	<div class="row">
	                	<div class="col-md-6">
	                	<div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="password" class="form-control" name="password" required>
	                            <label class="form-label">New Password</label>
	                        </div>
	                    </div>
	                    </div>
                    </div>
                	<div class="row">
                		<div class="col-md-6">
	                    <div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="password" class="form-control" name="password_confirmation" required>
	                            <label class="form-label">Confirm Password</label>
	                        </div>
	                    </div>
	                   </div>
                   </div>
                    <button class="btn btn-primary waves-effect" type="submit">Update Password</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
@endsection('content')