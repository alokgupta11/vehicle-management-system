@extends('layouts.app')
@section('title')
Orders List
@endsection('title')
@section('content')
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="header">
    <h2>
        <a href="{{url('orders')}}" class="btn bg-indigo waves-effect pull-right">
           <b> Orders List</b>
        </a>
        <h2>Edit Order - {{$order->order_uuid}}</h2>
    </h2>
</div>
<div class="body">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="{{ request()->edit == 'customer' ? 'active' : ''}}">
            <a href="{{url()->current().'?edit=customer'}}" aria-expanded="true">
                1.<i class="material-icons">person</i> Customer
            </a>
        </li>
        <li role="presentation" class="{{ request()->edit == 'vehicle' ? 'active' : ''}}">
            <a href="{{url()->current().'?edit=vehicle'}}" aria-expanded="false">
                2.<i class="material-icons">motorcycle</i> Vehicle Details
            </a>
        </li>
        <li role="presentation" class="{{ request()->edit == 'pick-drop' ? 'active' : ''}}">
            <a href="{{url()->current().'?edit=pick-drop'}}"  aria-expanded="false">
                3.<i class="material-icons">shopping_cart</i> Pickup/Delivery
            </a>
        </li>
        <li role="presentation" class="{{ request()->edit == 'vehicle-images' ? 'active' : ''}}">
            <a href="{{url()->current().'?edit=vehicle-images'}}" aria-expanded="false">
                4.<i class="material-icons">image</i>Preview Vehicle Images
            </a>
        </li>
    </ul>
    <br>
    @if(session('success_msg'))
      <div class="alert alert-success alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('success_msg') }}
      </div>
    @endif
    @if(session('error_msg'))
      <div class="alert alert-danger alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('error_msg') }}
      </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
                
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in {{ request()->edit == 'customer' ? 'active' : ''}}">
            <h5>Enter Customer Details</h5>
            <br>
            <p>
            <form class="form_validation" method="POST" action="{{route('admin_order_update',$order->order_uuid)}}">
            @csrf
            <input type="hidden" name="order_id" value="{{$order->id}}">
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="order_uuid" required value="{{$order->order_uuid}}" readonly="readonly">
                        <label class="form-label">Order ID</label>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control livesearch" name="customer_uuid" required  value="{{$order->customer_uuid}}">
                        <label class="form-label">Enter Customer ID</label>
                    </div>
                </div>
               </div>
           </div>
<!--            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div>Select Delivery type:</div>
                    <br>
                    <div class="">
                        <input name="type" id="radio_30" type="radio" class="with-gap radio-col-cyan" value="single" {{ ($order->type =="single")? "checked" : "" }}>
                        <label for="radio_30">Single Location</label>
                        <input name="type" id="radio_31" type="radio" class="with-gap radio-col-cyan" value="multiple" {{ ($order->type =="multiple")? "checked" : "" }}>
                        <label for="radio_31">Multiple Location</label>
                    </div>
                </div>
               </div>
           </div> -->
            <button class="btn btn-primary waves-effect" type="submit" name="action" value="customer">Update</button>
            <a class="btn btn-info waves-effect" href="{{url()->current().'?edit=vehicle'}}">Next</a>
            </form>
            </p>
        </div>
        <div role="tabpanel" class="tab-pane fade in {{ request()->edit == 'vehicle' ? 'active' : ''}}">
            <h5>Enter Vehicle Deatils</h5>
            <br>
            <p>
            <form class="form_validation" method="POST" action="{{route('admin_order_update',$order->order_uuid)}}">
            @csrf
            <input type="hidden" name="order_id" value="{{$order->id}}">
            <div class="row">
                <div class="col-md-6">
                <div class="">
                    <div class="">
                        <!-- <label class="form-label">Year</label> -->
                        <select class="form-control show-tick" name="year">
                            <option value="0">Select Year</option>
                            <option value="2021" {{ isset($vehicle->year) && $vehicle->year == "2021" ? 'selected':'' }}>2021</option>
                            <option value="2020" {{ isset($vehicle->year) && $vehicle->year == "2020" ? 'selected':'' }}>2020</option>
                            <option value="2019" {{ isset($vehicle->year) && $vehicle->year == "2019" ? 'selected':'' }}>2019</option>
                            <option value="2018" {{ isset($vehicle->year) && $vehicle->year == "2018" ? 'selected':'' }}>2018</option>
                            <option value="2017" {{ isset($vehicle->year) && $vehicle->year == "2017" ? 'selected':'' }}>2017</option>
                            <option value="2016" {{ isset($vehicle->year) && $vehicle->year == "2016" ? 'selected':'' }}>2016</option>
                            <option value="2015" {{ isset($vehicle->year) && $vehicle->year == "2015" ? 'selected':'' }}>2015</option>
                        </select>
                    </div>
                </div>
                </div>
                 <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="make" required value="{{isset($vehicle->make) ? $vehicle->make : old('make')}}">
                        <label class="form-label">Make</label>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="model" required value="{{isset($vehicle->model) ? $vehicle->model : old('model')}}">
                        <label class="form-label">Model</label>
                    </div>
                </div>
                </div>
                 <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="vin" required value="{{isset($vehicle->vin) ? $vehicle->vin : old('vin')}}">
                        <label class="form-label">VIN</label>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="stock" required value="{{isset($vehicle->stock) ? $vehicle->stock : old('stock')}}">
                        <label class="form-label">Stock</label>
                    </div>
                </div>
                </div>
                 <div class="col-md-6">
                 <div class="">
                    <div class="">
                        <!-- <label class="form-label">Year</label> -->
                        <select class="form-control show-tick" name="condition">
                            <option value="0">Select Condition</option>
                            <option value="OLD" {{ isset($vehicle->condition) && $vehicle->condition == "OLD" ? 'selected':'' }}>OLD</option>
                            <option value="NEW" {{ isset($vehicle->condition) && $vehicle->condition == "NEW" ? 'selected':'' }}>NEW</option>
                            <option value="DAMAGED" {{ isset($vehicle->condition) && $vehicle->condition == "DAMAGED" ? 'selected':'' }}>DAMAGED</option>
                        </select>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="">
                    <div class="">
                        <!-- <label class="form-label">Year</label> -->
                        <select class="form-control show-tick" name="is_keys">
                            <option value="0">Is Keys Present</option>
                            <option value="YES" {{ isset($vehicle->is_keys) && $vehicle->is_keys == "YES" ? 'selected':'' }}>Yes</option>
                            <option value="NO" {{ isset($vehicle->is_keys) && $vehicle->is_keys == "NO" ? 'selected':'' }}>NO</option>
                        </select>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <textarea name="comment" cols="30" rows="3" class="form-control no-resize" required>{{isset($vehicle->comment) ? $vehicle->comment : old('comment')}}</textarea>
                        <label class="form-label">Comment</label>
                    </div>
                </div>
               </div>
           </div>
            <button class="btn btn-primary waves-effect" type="submit" name="action" value="vehicle">Update</button>
            <a class="btn btn-info waves-effect" href="{{url()->current().'?edit=customer'}}">Prev</a>
            <a class="btn btn-info waves-effect" href="{{url()->current().'?edit=pick-drop'}}">Next</a>
            </p>
        </form>
        </div>

        <div role="tabpanel" class="tab-pane fade in {{ request()->edit == 'vehicle-images' ? 'active' : ''}}">
            <h5>Vehicle Images</h5>
            <br>
            <p>
                @if(isset($vehicle))
                @if(count($vehicle->images) > 0)
                <div class="sliderCont">
                    <div class="splide">
                        <div class="splide__track">
                            <ul class="splide__list">
                                @foreach($vehicle->images as $image)
                                <li class="splide__slide"><img src="{{url('vimages/'.$image->image_path)}}"></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @elseif(count($vehicle->images) == 0)
                <div class="alert alert-warning alert-dismissible" role="alert">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  No images uploaded yet!
                </div>
                @endif
                @else
                <div class="alert alert-warning alert-dismissible" role="alert">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Please fill vehicle details to start upload the images!
                </div>
                @endif
            </p>
        </div>

        <div role="tabpanel" class="tab-pane fade in {{ request()->edit == 'pick-drop' ? 'active' : ''}}">
            <p>
            @if(isset($pickup_delivery) && count($pickup_delivery) > 0)
            <h5>Pickup/Delivery Summary:</h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th>DriverId</th>
                            <th>PickupAddress</th>
                            <th>PickTime</th>
                            <th>DeliveryAddress</th>
                            <th>DeliveryTime</th>
                            <th>Status</th>
                            <th>CreatedBy</th>
                            <th>UpdatedBy</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pickup_delivery as $pd)
                        <tr>
                            <td>
                                <b>{{$pd->driver_uuid}}</b><br>
                            </td>
                            <td>
                                <p>{{$pd->pickup_address}}</p>
                                <p>{{$pd->pickup_location}}</p>
                                <p>{{$pd->pickup_zip}}</p>
                            </td>
                            <td>{{$pd->pickup_time}}</td>
                            <td>
                                <p>{{$pd->delivery_address}}</p>
                                <p>{{$pd->delivery_location}}</p>
                                <p>{{$pd->delivery_zip}}</p>
                            </td>
                            <td>{{$pd->delivery_time}}</td>
                            @if($pd->status == 'Driver UnAssigned')
                            <td><h5><label class="label bg-orange">{{$pd->status}}</label></h5></td>
                            @elseif($pd->status == 'Driver Assigned')
                            <td><h5><label class="label bg-cyan">{{$pd->status}}</label></h5></td>
                            @elseif($pd->status == 'Picked')
                            <td><h5><label class="label bg-blue">{{$pd->status}}</label></h5></td>
                            @elseif($pd->status == 'Delivered')
                            <td><h5><label class="label bg-green">{{$pd->status}}</label></h5></td>
                            @endif
                            <td>{{$pd->created_by}}</td>
                            <td>{{$pd->updated_by}}</td>
                            <td>
                                <a href="{{url('edit/order/'.$order->order_uuid.'?edit=pick-drop&id='.$pd->id.'&type=edit')}}" class="btn bg-cyan btn-xs waves-effect editPDLink"><i class="material-icons">edit</i></a>
                                @if(Auth::user()->role_id == 1)
                                 <a href="{{url('delete/pick-drop/'.$order->order_uuid.'/'.$pd->id)}}" class="btn bg-red btn-xs waves-effect deletePDLink" data-type="confirm"><i class="material-icons">delete</i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <div class="alert alert-warning alert-dismissible" role="alert">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              No drivers assigned to the order yet!
            </div>
            @endif
            <form class="form_validation" method="POST" action="{{route('admin_order_update',$order->order_uuid)}}">
            @csrf
            <input type="hidden" name="order_id" value="{{$order->id}}">
            @if($pd_type == 'edit')
            <input type="hidden" name="pick_deli_id" value="{{$pd_edit->id}}">
            @endif
            <h5>Enter Pickup/Delivery Details</h5>
           <h5>Select Driver:</h5>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        @if(Auth::user()->role_id == '1')
                        <input type="text" class="form-control livesearchDriver" name="driver_uuid" required value="{{isset($pd_edit->driver_uuid) ? $pd_edit->driver_uuid : old('driver_uuid')}}">
                        @elseif(Auth::user()->role_id == '2')
                        <input type="text" class="form-control" name="driver_uuid" readonly value="{{Auth::user()->user_uuid}}">
                        @endif
                        <label class="form-label">Enter Driver ID</label>
                    </div>
                </div>
                </div>
            </div>
            <h5>Enter PICKUP details:</h5>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="pickup_location" required value="{{isset($pd_edit->pickup_location) ? $pd_edit->pickup_location : old('pickup_location')}}">
                        <label class="form-label">Pickup Location</label>
                    </div>
                </div>
                </div>
               <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="pickup_zip" required value="{{isset($pd_edit->pickup_zip) ? $pd_edit->pickup_zip : old('pickup_zip')}}">
                        <label class="form-label">Pickup Zipcode</label>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <textarea class="form-control" name="pickup_address" required>{{isset($pd_edit->pickup_address) ? $pd_edit->pickup_address : old('pickup_address')}}</textarea>
                        <label class="form-label">Pickup Address</label>
                    </div>
                </div>
               </div>
               <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control datetimepicker" name="pickup_time" required value="{{isset($pd_edit->pickup_time) ? $pd_edit->pickup_time : old('pickup_time')}}">
                        <label class="form-label">Pickup Time</label>
                    </div>
                </div>
                </div>  
            </div>
            <h5>Enter DELIVERY details:</h5>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="delivery_location" required value="{{isset($pd_edit->delivery_location) ? $pd_edit->delivery_location : old('delivery_location')}}">
                        <label class="form-label">Delivery Location</label>
                    </div>
                </div>
                </div>
               <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="delivery_zip" required value="{{isset($pd_edit->delivery_zip) ? $pd_edit->delivery_zip : old('delivery_zip')}}">
                        <label class="form-label">Delivery Zipcode</label>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <textarea class="form-control" name="delivery_address" required>{{isset($pd_edit->delivery_address) ? $pd_edit->delivery_address : old('delivery_address')}}</textarea>
                        <label class="form-label">Delivery Address</label>
                    </div>
                </div>
               </div>
               <div class="col-md-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control datetimepicker" name="delivery_time" required value="{{isset($pd_edit->delivery_time) ? $pd_edit->delivery_time : old('delivery_time')}}">
                        <label class="form-label">Delivery Time</label>
                    </div>
                </div>
                </div>  
            </div>
            <div class="">
                <div>
                    <input type="checkbox" id="basic_checkbox_2" class="filled-in" name="is_final_destination" {{isset($pd_edit->is_final_destination) ? 'checked':''}} {{old('is_final_destination') ? 'checked' : ''}}> 
                    <label for="basic_checkbox_2"><b>Is Final Destination</b></label>
                </div>
                <p></p><br>
                <div class="confirmBlock {{isset($pd_edit->is_final_destination) ? 'show':'hide'}} {{old('is_final_destination') ? 'show' : 'hide'}}">
                   <h5>Enter Delivery confirmation Details</h5>
                    <p>
                       <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="delivery_person" required value="{{isset($order->delivery_person) ? $order->delivery_person : old('delivery_person')}}">
                                    <label class="form-label">Delivery person/Driver</label>
                                </div>
                            </div>
                            </div>
                      </div> 
                      <div class="row">
                           <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="receiver" required value="{{isset($order->receiver) ? $order->receiver : old('receiver')}}">
                                    <label class="form-label">Receiver name</label>
                                </div>
                            </div>
                           </div>
                           <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="condition" required value="{{isset($order->condition) ? $order->condition : old('condition')}}">
                                    <label class="form-label">Condition</label>
                                </div>
                            </div>
                           </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                        <div class="">
                            <div class="">
                                <label class="form-label">Signature</label>
                                <canvas id="signCanvas" width=420 height=200>
                                    
                                </canvas>
                                <!-- <button id="save">Save</button> -->
                            </div>
                            <br>
                            <div class="text-center">
                                <a class="btn btn-danger" id="clear">Clear</a>
                            </div>
                            <div>
                                @if(isset($order->signature))
                                 <img src="{{url('vimages/'.$order->signature)}}">
                                 <input type="hidden" id="signData" name="signature" value="signature.png">
                                @else
                                 <input type="hidden" id="signData" name="signature">
                                @endif
                            </div>
                            
                        </div>
                        </div>
                      </div>

                    </p>
                </div>
            </div>
            @if($pd_type == 'create')
            <button class="btn btn-primary waves-effect" type="submit" name="action" value="pick-drop">Add Pickup/Delivery Entry</button>
            @else
            <button class="btn btn-info waves-effect" type="submit" name="action" value="pick-drop-update">Update Pickup/Delivery Entry</button>
            @endif
            <a class="btn btn-info waves-effect" href="{{url()->current().'?edit=vehicle'}}">Prev</a>
            <p class="deliverBtnWrap {{isset($pd_edit->is_final_destination) ? 'show':'hide'}} {{old('is_final_destination') ? 'show' : 'hide'}}">
            OR
            <br>
            <button class="btn btn-success waves-effect deliverBtn" type="submit" name="action" value="confirm">Mark Delivered</button>
            </p>
            </form>
            <hr>
            <div class="imageUpload">
                <h5>Upload Vehicle Images</h5>
                <br>
                <p>
                    @if(isset($vehicle))
                    <form id="frmFileUpload" class="form_validation dropzone" method="POST" action="{{route('admin_vehicle_image_upload',$order->order_uuid)}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input type="hidden" name="vehicle_id" value="{{$vehicle->id}}">
                    <div class="dz-message">
                        <div class="drag-icon-cph">
                            <i class="material-icons">touch_app</i>
                        </div>
                        <h3>Drop files here or click to upload.</h3>
                    </div>
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                    </form>
                    @else
                    <div class="alert alert-warning alert-dismissible" role="alert">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      Please fill vehicle details to start upload the images!
                    </div>
                    @endif
                </p>
            </div>
            </p>
        </div>
    </div>
    </form>
</div>
</div>
</div>
</div>
@endsection