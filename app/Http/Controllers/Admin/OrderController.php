<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\UserInterface;
use App\Repositories\Interfaces\OrderInterface;
use Illuminate\Http\Request;
use Validator;
use Session;
use Hash;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderDeliveredCustomer;
use App\Mail\OrderDeliveredAdmin;
use App\Mail\AssignOrderToDriver;
use App\Mail\WelcomeEmailToCustomer;
use App\Mail\PickedOrderAdmin;
use App\Mail\OrderDeliveredByDriver;

class OrderController extends Controller
{
    protected $user;
    protected $order;

    public function __construct(UserInterface $user,OrderInterface $order)
    {
        $this->middleware('driver');
        $this->user = $user;
        $this->order = $order;
    }

    public function listOrders()
    {
        $pageType = 'admin_order_index';
        $orders = $order = $this->order->listOrders();

        return view('order.index',compact('pageType','orders'));
    }


    public function orderHitoryById($order_uuid)
    {
        $pageType = 'order_history_index';

        if(Auth::user()->role_id == 1){
        $orders = $order = $this->order->getOrderDetailsByOrderId($order_uuid);
        } else if(Auth::user()->role_id == 3){
            $orders = $order = $this->order->getOrderDetailsByOrderId($order_uuid,Auth::user()->user_uuid );
        }
        // echo 
        // echo $orders;
        return view('order.history',compact('pageType','orders'));
    }


    public function orderHistoryDetail($id)
    {
        $pageType = 'order_history_detail';

        
         $pickup = DB::table('pickup_delivery')
                ->select('*')->where("id",$id)
                ->first();

        // $images = DB::table('vehicle_images')
        //         ->select('*')->where("vehicle_id",$id)
        //         ->get();

        $images = $this->order->vimages($id);
        
        return view('order.history-detail',compact('pageType','pickup','images'));
    }


    public function listMyOrders()
    {
        $pageType = 'driver_order_index';
        $user = Auth::user();
        $orders = $this->order->listMyOrdersCopy($user);
        // $orders = $this->order->listMyOrders($user);

        return view('order.driver_index',compact('pageType','orders'));
    }

    public function customerSelectSearch(Request $request)
    {
        $customers = [];
        if($request->has('q')){
            $search = $request->q;
            $customers = $this->user->getCustomersSearchList($search);
        }
        return response()->json($customers);
    }


     public function SearchDelivery(Request $request)
    {
        $data = $request->all();
        $users=[];
        
        $users = DB::table('orders')
            ->select('to_location','to_address','to_zipcode')->where("order_uuid",$data['order_uuid'])
            ->get();

        return response()->json($users);

    }


    public function driverSelectSearch(Request $request)
    {
        $drivers = [];
        if($request->has('q')){
            $search = $request->q;
            $drivers = $this->user->getDriversSearchList($search);
        }
        return response()->json($drivers);
    }

    public function createOrder()
    {
        $pageType = 'admin_order_create';
        $order_uuid = unique_random_hash('orders','order_uuid',12);
        return view('order.create',compact('pageType','order_uuid'));
    }

    public function saveOrder(Request $request)
    {
        $data = $request->all();
        $vehi_data = [];
        $pd_data = [];

        if(Auth::user()->role_id == 1) {
             $rules = [
                'order_uuid' => 'required|size:12',
                'customer_uuid' => 'required|size:3',
            ];

        } else {
             $rules = [
            'order_uuid' => 'required|size:12',
            'customer_uuid' => 'required|size:3',
            'from_location' => 'required',
            'from_address' => 'required',
            'from_zipcode' => 'required',
            'to_location' => 'required',
            'to_address' => 'required',
            'to_zipcode' => 'required',
            'year' => 'required',
            'condition' => 'required',
            'is_keys' => 'required',
            'model' => "required",
            'vin' => "required",
            'make' => "required",
            'stock' => "required",
            ];   
        }
        
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        unset($data['_token']);

        /** Inititiating Transaction **/ 
        DB::beginTransaction();
        try {

            if(Auth::user()->role_id == 1){

                $data['status'] = 'Driver UnAssigned';
                $data['order_status'] = 0;
            } else if(Auth::user()->role_id == 2){

                $data['status'] = 'Driver Assigned';
                $data['order_status'] = 1;
                $data['first_driver']= Auth::user()->user_uuid;
            }
            
            $data['customer_uuid'] = $data['customer_uuid'];
            $data['from_location'] = $data['from_location'];
            $data['from_address'] = $data['from_address'];
            $data['from_zipcode'] = $data['from_zipcode'];
            $data['to_location'] = $data['to_location'];
            $data['to_address'] = $data['to_address'];
            $data['to_zipcode'] = $data['to_zipcode'];
            $data['created_by_id'] = Auth::user()->id;
            $data['created_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
            $order = $this->order->createOrder($data);


            /*** vehicle details array **/
            $vehi_data['order_id'] = $data['order_uuid'];
            $vehi_data['year'] = $data['year'];
            $vehi_data['make'] = $data['make'];
            $vehi_data['model'] = $data['model'];
            $vehi_data['vin'] = $data['vin'];
            $vehi_data['stock'] = $data['stock'];
            $vehi_data['condition'] = $data['condition'];
            $vehi_data['is_keys'] = $data['is_keys'];
            $vehi_data['comment'] = $data['comment'];

            $vehi_data['created_by_id'] = Auth::user()->id;
            $vehi_data['created_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
            $result = $this->order->createOrderVehicle($vehi_data);

        
        if(Auth::user()->role_id == 2) {

            $pd_data['order_id'] = $data['order_uuid'];
            $pd_data['driver_uuid'] = Auth::user()->user_uuid;
            $pd_data['pickup_location'] = $data['from_location'];
            $pd_data['pickup_zip'] = $data['from_zipcode'];
            $pd_data['pickup_address'] = $data['from_address'];
            $pd_data['created_by_id'] = Auth::user()->id;
            $pd_data['status'] = 'Driver Assigned';
            $pd_data['created_by'] = Auth::user()->first_name." ".Auth::user()->last_name;

            $result2 = $this->order->createOrderPickupDelivery($pd_data);

        }

            $order_id=$data['order_uuid'];
            $order_data = $this->order->getOrderDetails($order_id);
            $user = $this->user->getUserByUUID($data['customer_uuid']);
           

           Mail::to($user->email)->send(new WelcomeEmailToCustomer($order_data,$user));
            

            DB::commit();

            Session::flash('success_msg','Order created successfully.');
            // return redirect()->to('orders');
                if(Auth::user()->role_id == '1')
                {
                    return redirect()->to('orders');
                }
                elseif(Auth::user()->role_id == '2')
                {
                    return redirect()->to('my-orders');
                }


        } catch (\Exception $e) {
            DB::rollback();
            echo $e;
            // something went wrong
            Session::flash('error_msg','something Went wrong. Please try again');
            //return redirect()->to('create/order');
        }
        
    }

    /*********** assign driver **********************/
    public function assignDriver(Request $request, $order_uuid)
    {
        $pageType = 'admin_order_edit';
        $order = $this->order->getOrderDetails($order_uuid);

        // echo "<pre>";
        // print_r($last_address);

        $vehicle = $this->order->getOrderVehicleDetails($order_uuid);
        if($vehicle)
        {
            $vehicle->images = $this->getVehicleImages($order_uuid,$vehicle->id);
        }
        $pickup_delivery = $this->order->getOrderPickupDeliveryDetails($order_uuid);

        if(isset($request->id))
        {
            $pd_edit = $this->order->getOrderPickupDeliveryDetailById($request->id);
            $pd_type = 'edit';
        }
        else
        {
            $pd_edit = collect();
            $pd_type = 'create';
        }
        if(count($pickup_delivery) == 0)
        {
            $show_confirm = false;
        }
        else
        {
            $show_confirm = true;
            foreach($pickup_delivery as $pd)
            {
                if(is_null($pd->date_time))
                {
                  $show_confirm = false;
                  break;
                }
            }
        }

    
        return view('order.assign-driver',compact('pageType','order','vehicle','pickup_delivery','pd_edit','pd_type','show_confirm'));
    }


    /************************** re assign order ***********************/

    public function reAssignDriver(Request $request, $order_uuid)
    {
        $pageType = 'admin_order_edit';
        $order = $this->order->getOrderDetails($order_uuid);

        $last_delivery_destination=$this->order->getlastPickupDeliveryAddress($order_uuid);

        // print_r($lastDeliveryDestination);
        $vehicle = $this->order->getOrderVehicleDetails($order_uuid);
        if($vehicle)
        {
            $vehicle->images = $this->getVehicleImages($order_uuid,$vehicle->id);
        }
        $pickup_delivery = $this->order->getOrderPickupDeliveryDetails($order_uuid);

        if(isset($request->id))
        {
            $pd_edit = $this->order->getOrderPickupDeliveryDetailById($request->id);
            $pd_type = 'edit';
        }
        else
        {
            $pd_edit = collect();
            $pd_type = 'create';
        }
        if(count($pickup_delivery) == 0)
        {
            $show_confirm = false;
        }
        else
        {
            $show_confirm = true;
            foreach($pickup_delivery as $pd)
            {
                if(is_null($pd->date_time))
                {
                  $show_confirm = false;
                  break;
                }
            }
        }
        
        $last_address = $this->order->getlastAddress($order_uuid);
    
        return view('order.reassign-driver',compact('pageType','order','vehicle','pickup_delivery','pd_edit','pd_type','show_confirm','last_address','last_delivery_destination'));
    }


    public function editOrder(Request $request, $order_uuid)
    {
        $pageType = 'admin_order_edit';
        $order = $this->order->getOrderDetails($order_uuid);
        $vehicle = $this->order->getOrderVehicleDetails($order_uuid);
        if($vehicle)
        {
            $vehicle->images = $this->getVehicleImages($order_uuid,$vehicle->id);
        }
        $pickup_delivery = $this->order->getOrderPickupDeliveryDetails($order_uuid);
        if(isset($request->id))
        {
            $pd_edit = $this->order->getOrderPickupDeliveryDetailById($request->id);
            $pd_type = 'edit';
        }
        else
        {
            $pd_edit = collect();
            $pd_type = 'create';
        }
        if(count($pickup_delivery) == 0)
        {
            $show_confirm = false;
        }
        else
        {
            $show_confirm = true;
            foreach($pickup_delivery as $pd)
            {
                if(is_null($pd->date_time))
                {
                  $show_confirm = false;
                  break;
                }
            }
        }
    
        return view('order.create',compact('pageType','order','vehicle','pickup_delivery','pd_edit','pd_type','show_confirm'));
    }

    


    /**********************update orer details *********************/
    public function updateOrder2(Request $request,$order_uuid)
    {
        $data = $request->all();

        // $pd_curr = $this->order->getOrderPickupDeliveryById($data['pick_deli_id']);
        
    if($data['action'] == 'add')
        {
            $pd_data = [];
            $order_data = [];

         /** Inititiating Transaction **/ 
        DB::beginTransaction();
        try{

            $rules = [
                'driver_uuid' => 'required|not_in:0',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);
            $pd_data['order_id'] = $data['order_uuid'];
            $pd_data['pickup_location'] = $data['pickup_location'];
            $pd_data['pickup_zip'] = $data['pickup_zip'];
            $pd_data['pickup_address'] = $data['pickup_address'];

            $pd_data['delivery_location'] = $data['delivery_location'];
            $pd_data['delivery_zip'] = $data['delivery_zip'];
            $pd_data['delivery_address'] = $data['delivery_address'];

            $pd_data['driver_uuid'] = $data['driver_uuid'];
            $pd_data['pickup_time'] = $data['pickup_time'];
            $pd_data['delivery_time'] = $data['delivery_time'];

            $pd_data['is_final_destination'] = isset($data['is_final_destination']) ? $data['is_final_destination'] : null;

            $order_status = 'Driver UnAssigned';
            $ostatus = 0;
            
            if(!is_null($data['driver_uuid']))
            {

                $order_data['from_location']=$data['pickup_location'];
                $order_data['from_address']=$data['pickup_address'];
                $order_data['from_zipcode']=$data['pickup_zip'];
                
                $order_data['status'] = 'Driver Assigned';
                $ostatus = 1;

                $order_status = 'Driver Assigned';
                $pd_data['status'] = 'Driver Assigned';
            }
            else
            {
                $pd_data['status'] = 'Driver UnAssigned';
                $order_status = 'Driver UnAssigned';
            }

            if($data['first_driver'] == '' ){
                $order_data['first_driver']=$data['driver_uuid'];
            }

            $order_data['order_uuid']=$order_uuid;
            $order_data['status']=$order_status;
            $order_data['order_status']=$ostatus;
            $order_data['updated_by_id']=Auth::user()->id;
            $order_data['updated_by']=Auth::user()->first_name." ".Auth::user()->last_name;

            
            
            $this->order->updateOrderStatus($order_data);

            $pd_data['created_by_id'] = Auth::user()->id;
            $pd_data['created_by'] = Auth::user()->first_name." ".Auth::user()->last_name;

            $result = $this->order->createOrderPickupDelivery($pd_data);


            $driver = $this->user->getUserByUUID($data['driver_uuid']);
            $order_details = $this->order->getOrderDetails($order_uuid);

            if(!is_null($data['driver_uuid']))
            {
                Mail::to($driver->email)->send(new AssignOrderToDriver($order_details,$driver));
            }

            DB::commit();

            Session::flash('success_msg','Order: Pickup/Delivery details created successfully.');
            return redirect()->to('orders');

            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
                Session::flash('error_msg','something Went wrong. Please try again');
                return redirect()->to('orders');
            }


        }


        /************************************* IF Order Marked as completed ***************************/

        if($request->input('action') == 'confirm')
        {
            $order_data = [];
            $rules = [
                'driver_uuid' => 'required',
                'pickup_location' => 'required',
                'pickup_zip' => 'required',
                'pickup_address' => 'required',
                'pickup_time' => 'required',
                'delivery_location' => 'required',
                'delivery_zip' => 'required',
                'delivery_address' => 'required',
                'delivery_time' => 'required',
                'delivery_person' => 'required',
                'receiver' => 'required',
                'condition' => 'required',
                'signature' => 'required'
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);

            $order_data['order_uuid'] = $order_uuid;
            $order_data['delivery_person'] = $data['delivery_person'];
            $order_data['receiver'] = $data['receiver'];
            $order_data['condition'] = $data['condition'];


            $data_uri = $data['signature'];
            if($data['signature'] != 'signature.png')
            {
                $encoded_image = explode(",", $data_uri)[1];
                $decoded_image = base64_decode($encoded_image);
                $sign_path = $order_uuid."/signature.png";
                $path = public_path().'/vimages/'.$order_uuid;
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                file_put_contents(base_path().'/public/vimages/'.$sign_path, $decoded_image);
                $order_data['signature'] = $sign_path;
            }
            $order_data['status'] = 'Delivered';
            $order_data['updated_by_id'] = Auth::user()->id;
            $order_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
            $result = $this->order->updateOrderCustomer($order_data);

            $order = $this->order->getOrderDetails($order_uuid);
            $user = $this->user->getUserByUUID($order->customer_uuid);

            Mail::to($user->email)->send(new OrderDeliveredCustomer($order,$user));
            Mail::to(env('MAIL_TO_ADDRESS'))->send(new OrderDeliveredAdmin($order,$user));
            

            if(Auth::user()->role_id == '1')
            {
                Session::flash('success_msg','Order: Delivered successfully.');
                return redirect()->to('orders');
            }
            elseif(Auth::user()->role_id == '2')
            {
                Session::flash('success_msg','Order: Delivered successfully.');
                return redirect()->to('my-orders');
            }
            
        }


    }   




    //************************** UDPATE ORDER details **************************/
      public function updateOrderDetails(Request $request)
    {
        $data = $request->all();


        /************************************* IF Order Marked as completed ***************************/

        if($request->input('action') == 'update-order')
        {
            $order_data = [];
            $vehi_data=[];
            
            if(Auth::user()->role_id == 1) {
             $rules = [
                'order_uuid' => 'required|size:12',
                'customer_uuid' => 'required|size:3',
            ];

            } else {
                 $rules = [
                'order_uuid' => 'required|size:12',
                'customer_uuid' => 'required|size:3',
                'from_location' => 'required',
                'from_address' => 'required',
                'from_zipcode' => 'required',
                'to_location' => 'required',
                'to_address' => 'required',
                'to_zipcode' => 'required',
                'year' => 'required',
                'condition' => 'required',
                'is_keys' => 'required',
                'model' => "required",
                'vin' => "required",
                'make' => "required",
                'stock' => "required",
                ];   
            }

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);

            $order_data['order_uuid'] = $data['order_uuid'];

            $order_data['from_location'] = $data['from_location'];
            $order_data['from_address'] = $data['from_address'];
            $order_data['from_zipcode'] = $data['from_zipcode'];
            $order_data['to_location'] = $data['to_location'];
            $order_data['to_address'] = $data['to_address'];
            $order_data['to_zipcode'] = $data['to_zipcode'];



            $order_data['updated_by_id'] = Auth::user()->id;
            $order_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
            $result = $this->order->updateOrderCustomer($order_data);



            /*** vehicle details array **/
            $vehi_data['order_id'] = $data['order_uuid'];
            $vehi_data['year'] = $data['year'];
            $vehi_data['make'] = $data['make'];
            $vehi_data['model'] = $data['model'];
            $vehi_data['vin'] = $data['vin'];
            $vehi_data['stock'] = $data['stock'];
            $vehi_data['condition'] = $data['condition'];
            $vehi_data['is_keys'] = $data['is_keys'];
            $vehi_data['comment'] = $data['comment'];

            $vehi_data['updated_by_id'] = Auth::user()->id;
            $vehi_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;

            $vresut = $this->order->updateVehicleDetails($vehi_data);

            
            if(Auth::user()->role_id == '1')
            {
                Session::flash('success_msg','Order: Delivered successfully.');
                return redirect()->to('orders');
            }
            elseif(Auth::user()->role_id == '2')
            {
                Session::flash('success_msg','Order: Updated successfully.');
                return redirect()->to('my-orders');
            }
            
        }


    }   


/****************************** EDIT SIPMENT DETAILS *****************************/
 /*********** assign driver **********************/
    public function editShipment(Request $request, $id,$order_uuid)
    {
        $pageType = 'admin_order_edit';
        $order = $this->order->getOrderDetails($order_uuid);

        // echo "<pre>";
        // print_r($last_address);

        $vehicle = $this->order->getOrderVehicleDetails($order_uuid);
        if($vehicle)
        {
            $vehicle->images = $this->getVehicleImages($order_uuid,$vehicle->id);
        }
        $pickup_delivery = $this->order->getOrderPickupDeliveryDetails($order_uuid);

        $pd_edit = $this->order->getOrderPickupDeliveryDetailById($id);

        if(count($pickup_delivery) == 0)
        {
            $show_confirm = false;
        }
        else
        {
            $show_confirm = true;
            foreach($pickup_delivery as $pd)
            {
                if(is_null($pd->date_time))
                {
                  $show_confirm = false;
                  break;
                }
            }
        }

    
        return view('order.edit-shipment',compact('pageType','order','vehicle','pickup_delivery','pd_edit','show_confirm'));
    }


/******************************** UPDATE SHIPMENT DETAILS *****************************/

 public function updateShipment(Request $request,$order_uuid)
    {
        $data = $request->all();

        // $pd_curr = $this->order->getOrderPickupDeliveryById($data['pick_deli_id']);
        
    if($data['action'] == 'update')
        {
            $pd_data = [];
            $order_data = [];

         /** Inititiating Transaction **/ 
        DB::beginTransaction();
        try{

        if($data['pickup_time'] != ''){
            $rules = [
                'driver_uuid' => 'required|not_in:0',
                'pickup_location' => 'required',
                'pickup_zip' => 'required',
                'pickup_address' => 'required',
                'pickup_time' => 'required',
            ];
        } 
        if(!is_null($data['delivery_time'])){
             $rules = [
                'driver_uuid' => 'required|not_in:0',
                'pickup_location' => 'required',
                'pickup_zip' => 'required',
                'pickup_address' => 'required',
                'pickup_time' => 'required',
                'delivery_location' => 'required',
                'delivery_zip' => 'required',
                'delivery_address' => 'required',
                'delivery_time' => 'required',
                
            ];
        } 

        // if(!is_null($data['is_final_destination'])){
        //      $rules = [
        //         'driver_uuid' => 'required|not_in:0',
        //         'pickup_location' => 'required',
        //         'pickup_zip' => 'required',
        //         'pickup_address' => 'required',
        //         'pickup_time' => 'required',
        //         'delivery_location' => 'required',
        //         'delivery_zip' => 'required',
        //         'delivery_address' => 'required',
        //         'delivery_time' => 'required',
                
        //     ];
        // } 
        // else {
        //      $rules = [
        //         'driver_uuid' => 'required|not_in:0',
        //     ];
        // }
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);

            $vehicle_details_uploaded=0;
            $vehicle_images_uploaded=0;

            $vehicle = $this->order->checkVehicleDataExist($order_uuid);
            $vehicleImages = $this->order->checkVehicleImageDataExist($order_uuid,$data['driver_uuid']);
            /******************** if images are already uploaded *******/
           
            if($vehicle[0]['year'] != '' && $vehicle[0]['make'] != '' && $vehicle[0]['model'] != '' && $vehicle[0]['condition'] && $vehicle[0]['is_keys'] != '' )
            {
                $vehicle_details_uploaded=1;
            }

            if($vehicleImages > 0 )
            {
                $vehicle_images_uploaded=1;
            }

            if($vehicle_details_uploaded == 0 ){
                Session::flash('error_msg','Please Update Vehicle Details First');
                return redirect()->to('my-orders');

            } else if($vehicle_images_uploaded == 0 ){
                Session::flash('error_msg','Please Upload Images');  
                //echo $vehicle[0]['year'];
                 return redirect()->to('update/shipment/'.$data['pickup_delivery_id'].'/'.$order_uuid.'?act=update');
            } 
            else if($vehicle_details_uploaded == 1 && $vehicle_images_uploaded == 1){

                    $pd_data['id'] = $data['pickup_delivery_id'];
                    $pd_data['order_id'] = $data['order_uuid'];
                    $pd_data['pickup_location'] = $data['pickup_location'];
                    $pd_data['pickup_zip'] = $data['pickup_zip'];
                    $pd_data['pickup_address'] = $data['pickup_address'];
                    $pd_data['pickup_time'] = $data['pickup_time'];

                    $pd_data['delivery_location'] = $data['delivery_location'];
                    $pd_data['delivery_zip'] = $data['delivery_zip'];
                    $pd_data['delivery_address'] = $data['delivery_address'];
                    $pd_data['delivery_time'] = $data['delivery_time'];

                    $pd_data['driver_uuid'] = $data['driver_uuid'];
                    $pd_data['pickup_time'] = $data['pickup_time'];
                    $pd_data['delivery_time'] = $data['delivery_time'];

                    $pd_data['is_final_destination'] = $data['is_final_destination'];

                    $order_status = 'Picked';
                    $ostatus = 3;
                    

                    $pd_data['status'] = $order_status;

                    $order_data['order_uuid']=$order_uuid;
                    $order_data['status']=$order_status;
                    $order_data['order_status']=$ostatus;
                    $order_data['updated_by_id']=Auth::user()->id;
                    $order_data['updated_by']=Auth::user()->first_name." ".Auth::user()->last_name;
                    
                    
                     $this->order->updateOrderStatus($order_data);

                    $pd_data['updated_by_id'] = Auth::user()->id;
                    $pd_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;

                    $result = $this->order->updateOrderPickupDelivery($pd_data);


                    $order_details = $this->order->getOrderDetails($order_uuid);
                    $user = $this->user->getUserByUUID($order_details->customer_uuid);

                    Mail::to(env('MAIL_TO_ADDRESS'))->send(new PickedOrderAdmin($order_details,$user));


                    DB::commit();

                    Session::flash('success_msg','Order: Picked Up successfully.');
                    return redirect()->to('my-orders');

            } 
            

             

            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
                  //echo $e;
                //echo $vehicle->year;
                Session::flash('error_msg','something Went wrong. Please try again');
                return redirect()->to('update/shipment/'.$data['pickup_delivery_id'].'/'.$order_uuid.'?act=update');
            }


        }


        /************************************* IF Order Marked as completed ***************************/

        if($request->input('action') == 'confirm')
        {
            $order_data = [];
            $pd_data = [];
            $rules = [
                'driver_uuid' => 'required',
                'pickup_location' => 'required',
                'pickup_zip' => 'required',
                'pickup_address' => 'required',
                'pickup_time' => 'required',
                'delivery_location' => 'required',
                'delivery_zip' => 'required',
                'delivery_address' => 'required',
                'delivery_time' => 'required',
                'delivery_person' => 'required',
                'receiver' => 'required',
                'condition' => 'required',
                'signature' => 'required'
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
            unset($data['_token']);

            $vehicleImages = $this->order->checkVehicleImageDataExist($order_uuid,$data['driver_uuid']);
            /******************** if images are already uploaded *******/
            if($vehicleImages > 0 )
            {
                
                $order_data['order_uuid'] = $order_uuid;
                $pd_data['delivery_person'] = $data['delivery_person'];
                $pd_data['receiver'] = $data['receiver'];
                $pd_data['ocondition'] = $data['condition'];


                $order_data['delivery_person'] = $data['delivery_person'];
                $order_data['receiver'] = $data['receiver'];
                $order_data['condition'] = $data['condition'];


                $data_uri = $data['signature'];
                if($data['signature'] != 'signature.png')
                {
                    $encoded_image = explode(",", $data_uri)[1];
                    $decoded_image = base64_decode($encoded_image);
                    $sign_path = $order_uuid."/signature".$data['pickup_delivery_id'].".png";
                    $path = public_path().'/vimages/'.$order_uuid;
                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                    file_put_contents(base_path().'/public/vimages/'.$sign_path, $decoded_image);
                    $pd_data['signature'] = $sign_path;
                }

               

                if($data['is_final_destination'] == 1 ){
                    $order_data['order_status'] = 4;
                    $order_data['status'] = 'Delivered by Driver';
                } else {
                    $order_data['order_status'] = 3;
                    $order_data['status'] = 'Delivered- Waiting to Reassign';
                }
                
                $order_data['updated_by_id'] = Auth::user()->id;
                $order_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
                $result = $this->order->updateOrderCustomer($order_data);

                
                $pd_data['id'] = $data['pickup_delivery_id'];
                $pd_data['status'] = 'Delivered';
                $pd_data['updated_by_id'] = Auth::user()->id;
                $pd_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;

                $result = $this->order->updateOrderPickupDelivery($pd_data);

                $order = $this->order->getOrderDetails($order_uuid);
                $user = $this->user->getUserByUUID($order->customer_uuid);

                Mail::to(env('MAIL_TO_ADDRESS'))->send(new OrderDeliveredByDriver($order,$user));

                Session::flash('success_msg','Order: Delivered successfully.');
            }
             else {
                Session::flash('error_msg','Order: Pickup Images are not uploaded yet. Please uplaod and try again.');
             }
            
            
            
            if(Auth::user()->role_id == '1')
            {
                return redirect()->to('orders');
            }
            elseif(Auth::user()->role_id == '2')
            {
                return redirect()->to('my-orders');
            }
            
        }


        /************************************* IF Order Marked as completed ***************************/

        if($request->input('action') == 'confirm-delivery')
        {

            echo "string";

        }

    }   



    public function uploadVehicleImages(Request $request,$order_uuid)
    {
        $order_id = $request->order_id;
        $vehicle_id = $request->vehicle_id;
        if($request->hasFile('file'))
        {
            $image = $request->file('file');
            if ($image->isValid()) {
                $image_name = Str::random(14).".". $image->getClientOriginalExtension();
                $base_path = base_path().'/public/vimages/';
                $db_path = $order_uuid."/".$image_name;
                $path = base_path().'/public/vimages/'.$order_uuid;
                $image->move($path,$image_name);
                $vehi_image = ['vehicle_id' => $vehicle_id,
                                'order_uuid' => $order_uuid,
                               'driver_id' => Auth::user()->id,
                               'driver_uuid' => Auth::user()->user_uuid,
                               'image_name' => $image_name,
                              'image_path' => $db_path,
                              'created_by_id' => Auth::user()->id,
                              'created_by' => Auth::user()->first_name." ".Auth::user()->last_name,
                              'updated_by_id' => Auth::user()->id,
                              'updated_by' => Auth::user()->first_name." ".Auth::user()->last_name,
                             ];
                $result = $this->order->createOrderVehicleImage($vehi_image);
                // $log['order_id'] = $order_uuid;
                // $log['user_id'] = Auth::user()->id;
                // $log['action'] = 'CREATE';
                // $curr_user = current_user();
                // $log['message'] = "$curr_user uploaded image to the order #$order_uuid";
                // activity_logg($log);
            }
        }
        
        return $image_name;
    }

    public function getVehicleImages($order_uuid,$vehicle_id)
    {
       if(Auth::user()->role_id == '1')
       {
         $images = $this->order->getVehicleImages($vehicle_id);
       }
       else
       {
         $images = $this->order->getVehicleImagesByDriver($vehicle_id,Auth::user()->id);
       }
       
       return $images;
    }

    public function deleteVehicleImage(Request $request,$order_uuid)
    {
       $file_name = $request->file_name;
       $path = public_path()."/vimages/".$order_uuid."/".$file_name;
       if(File::exists($path))
       {
         File::delete($path);
       }
       $result = $this->order->deleteVehicleImage($order_uuid,$file_name);
        $log['order_id'] = $order_uuid;
        $log['user_id'] = Auth::user()->id;
        $log['action'] = 'DELETE';
        $curr_user = current_user();
        $log['message'] = "$curr_user deleted image from the order #$order_uuid";
       return $result;
    }

    public function deleteOrder($order_uuid)
    {
       $order_id = $this->order->getOrderIdByUuid($order_uuid);
       $vehicle_id = $this->order->getVehicleIdByOrderId($order_id);
       $images = $this->order->getVehicleImages($vehicle_id);
       if(count($images) > 0)
       {
           foreach($images as $image)
           {
               $path = public_path()."/vimages/".$image->image_path;
               if(File::exists($path))
               {
                 File::delete($path);
               }
           }
       }
       $result = $this->order->deleteAllVehicleImagesByVehicleId($vehicle_id);
       $result = $this->order->deleteVehicleDetailByVehicleId($vehicle_id);
       $result = $this->order->deleteAllOrderPickupDelivery($order_uuid);
       $result = $this->order->deleteOrder($order_uuid);

        $log['order_id'] = $order_uuid;
        $log['user_id'] = Auth::user()->id;
        $log['action'] = 'DELETE';
        $curr_user = current_user();
        $log['message'] = "$curr_user deleted the Order #$order_uuid";
        activity_logg($log);
       Session::flash('success_msg','Order details deleted successfully.');
       return redirect()->back();
    }

    public function deleteOrderPickDelivery($order_uuid,$pd_id)
    {
        $result = $this->order->deleteOrderPickDelivery($order_uuid,$pd_id);
        $log['order_id'] = $order_uuid;
        $log['user_id'] = Auth::user()->id;
        $log['action'] = 'DELETE';
        $curr_user = current_user();
        $log['message'] = "$curr_user deleted the Pickup/delivery Entry for Order #$order_uuid";
        activity_logg($log);
        Session::flash('success_msg','Order: Pickup/Delivery entry deleted successfully.');
        return redirect()->back();
    }


    public function confirm_delivery($order_uuid)
    {
        $order_data=[];

        $order_data['order_uuid']=$order_uuid;
        $order_data['order_status']=5;
        $order_data['status']="Delivered";
        $order_data['updated_by_id'] = Auth::user()->id;
        $order_data['updated_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
        $result = $this->order->updateOrderCustomer($order_data);

        $order = $this->order->getOrderDetails($order_uuid);
                $user = $this->user->getUserByUUID($order->customer_uuid);

        Mail::to($user->email)->send(new OrderDeliveredCustomer($order,$user));
        Mail::to(env('MAIL_TO_ADDRESS'))->send(new OrderDeliveredAdmin($order,$user));

       Session::flash('success_msg','Order Delivered successfully.');
       return redirect()->back();
    }


}