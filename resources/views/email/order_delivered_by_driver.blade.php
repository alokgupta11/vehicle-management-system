@component('mail::message')

<p>Dear Admin!</p>

<p>Order <b>#{{$order->order_uuid}}</b> for customer <b>#{{$user->user_uuid}} {{$user->first_name}}  {{$user->last_name}}</b> has been successfully Delivered by driver on {{$order->updated_at}} </p>
<p><b>Delivery Person: </b> {{$order->delivery_person}}</p>
<p><b>Receiver name: </b> {{$order->receiver}}</p>
<p>Please login into admin account for more details</p>

<p></p>
Thanks & Regards,<br>
The Support team,<br>
<b>{{ config('app.name') }}</b>
@endcomponent