@extends('layouts.app')
@section('title')
Customers
@endsection('title')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
            	<a href="{{url('create/customer')}}" class="btn bg-indigo waves-effect pull-right">
                   <b>Add Customer</b>
                </a>
                <h2>Customers List</h2>
            </div>
            <div class="body">
			<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
			    <thead>
			        <tr>
			        	<th>CustomerID</th>
			            <th>Name</th>
			            <th>Phone</th>
			            <th>Email</th>
			            <th>Address</th>
			            <th>CreatedDate</th>
			            <th>LastEditBy</th>
			            <th>Actions</th>
			        </tr>
			    </thead>
			    <tbody>
			    	@foreach($customers as $cust)
			        <tr>
			            <td>{{$cust->user_uuid}}</td>
			            <td>{{$cust->first_name}} {{$cust->last_name}}</td>
			            <td>{{$cust->phone}}</td>
			            <td>{{$cust->email}}</td>
			            <td>{{$cust->address}}</td>
			            <td>{{$cust->created_at}}</td>
			            <td>{{$cust->last_edit_by}}</td>
			            <td>
			            	<a href="{{url('edit/customer/'.$cust->user_uuid)}}" class="btn bg-cyan btn-xs waves-effect"><i class="material-icons">edit</i></a>
                            @if(Auth::user()->role_id == '1')
                            <a href="{{url('delete/customer/'.$cust->user_uuid)}}" class="btn bg-red btn-xs waves-effect deleteUserLink" data-type="confirm"><i class="material-icons">delete</i></a>
			                @endif
			            </td>
			        </tr>
			        @endforeach
			    </tbody>
			</table>
			</div>
		</div>
</div>
</div>
</div>
@endsection('content')