@extends('layouts.app')
@section('title')
My Orders List
@endsection('title')
@section('content')
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="header">
    <h2>
        My Orders List
        <a href="{{url('create/order')}}" class="btn bg-indigo waves-effect pull-right">
           <b>Create Order</b>
        </a>
    </h2>
</div>
@if(session('success_msg'))
      <div class="alert alert-success alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('success_msg') }}
      </div>
    @endif

@if(session('error_msg'))
      <div class="alert alert-danger alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('error_msg') }}
      </div>
    @endif
                <div class="body">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                <thead>
                    <tr>
                        <th>OrderID</th>
                        <th>Pickup Details</th>
                        <th>Status</th>
                        <th>CreatedBy</th>
                        <th>Created on</th>
                        <th>UpdatedBy</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $ord)
                    <tr>
                        <td><b>{{$ord->order_id}}</b></td>
                        <td><b>{{$ord->pickup_location}}, {{$ord->pickup_address}}</b><br>{{$ord->pickup_zip}}
                        </td>
                        @if($ord->status == 'Driver UnAssigned')
                        <td><h5><label class="label bg-orange">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Driver Assigned')
                        <td><h5><label class="label bg-cyan">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'In Transit')
                        <td><h5><label class="label bg-blue">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Delivered')
                        <td><h5><label class="label bg-green">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Picked')
                        <td><h5><label class="label bg-orange">{{$ord->status}}</label></h5></td>
                        @else
                        <td></td>
                        @endif
                        <td>{{$ord->created_by}}</td>
                        <td>{{$ord->created_at}}</td>
                        <td>{{$ord->updated_by}} </td>
                        <td>
                            @if($ord->status != 'Delivered')



                                <a href="{{url('edit/order/'.$ord->order_id.'?act=edit-order')}}" class="btn bg-cyan btn-xs waves-effect" title="Edit Order Details"><i class="material-icons" title="Edit Order Details">edit</i></a>
                         

                               

                                <a href="{{url('update/shipment/'.$ord->id.'/'.$ord->order_id.'?act=update')}}" class="btn bg-orange btn-xs waves-effect" title="Update Order status"><i class="material-icons" title="Update Order status">local_shipping</i></a>

                                @if($ord->status == 'Picked')
                                    <a href="{{url('update/shipment/'.$ord->id.'/'.$ord->order_id.'?act=confirm-order')}}" class="btn bg-green btn-xs waves-effect" title="Confirm Delivery"><i class="material-icons" title="Confirm Delivery">check_circle</i></a>
                                @endif

                                @if(Auth::user()->role_id == '1')
                                <a href="{{url('delete/order/'.$ord->order_uuid)}}" class="btn bg-red btn-xs waves-effect deleteOrderLink" data-type="confirm"><i class="material-icons">delete</i></a>
                                @endif
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
</div>
</div>
</div>
@endsection('content')