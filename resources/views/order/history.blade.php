@extends('layouts.app')
@section('title')
Orders History
@endsection('title')
@section('content')
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="header">
    <h2>
        Orders History
        <!-- <a href="{{url('create/order')}}" class="btn bg-indigo waves-effect pull-right">
           <b>Create Order</b>
        </a> -->
    </h2>
</div>

@if(session('success_msg'))
      <div class="alert alert-success alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('success_msg') }}
      </div>
    @endif

            <div class="body">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                <thead>
                    <tr>
                        <th>OrderID</th>
                        <th>Status</th>
                        <th>Driver Id</th>
                        <!-- <th>Pickup Location</th>
                        <th>Pickup Address</th>
                        <th>Pickup Zip</th> -->
                        <th>Pickup Time</th>
                        <!-- <th>Delivery location</th>
                        <th>Delivery Address</th>
                        <th>Delivery Zip</th> -->
                        <th>Delivery Time</th>
                        
                        <th>Created on</th>
                        <th>Updated on</th>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $ord)
                    <tr>
                        <td><b>{{$ord->order_id}}</b></td>
                        @if($ord->status == 'Picked')
                        <td><h5><label class="label bg-orange">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Driver Assigned')
                        <td><h5><label class="label bg-cyan">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'In Transit')
                        <td><h5><label class="label bg-blue">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Delivered')
                        <td><h5><label class="label bg-green">{{$ord->status}}</label></h5></td>
                        @endif
                        <td>{{$ord->driver_uuid}}</td>
                        <!-- <td>{{$ord->pickup_location}}</td> -->
                        <!-- <td>{{$ord->pickup_address}}</td>
                        <td>{{$ord->pickup_zip}}</td> -->
                        <td>{{$ord->pickup_time}}</td>
                        <!-- <td>{{$ord->delivery_location}}</td>
                        <td>{{$ord->delivery_address}}</td>
                        <td>{{$ord->delivery_zip}}</td> -->
                        <td>{{$ord->delivery_time}}</td>
                        
                        <td>{{$ord->created_at}}</td>
                        <td>{{$ord->updated_at}}</td>
                        <td>
                            <a href="{{url('order/history_detail/'.$ord->id.'')}}" class="btn bg-green btn-xs waves-effect" title="View history"><i class="material-icons" title="View history">visibility</i></a>
                        </td>
                       
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
</div>
</div>
</div>
@endsection('content')