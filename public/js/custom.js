$(document).ready(function(){

$('#form_validation,.form_validation').validate({
rules: {
    'name': {
        required: true
    },
    'gender': {
        required: true
    }
},
highlight: function (input) {
    $(input).parents('.form-line').addClass('error');
},
unhighlight: function (input) {
    $(input).parents('.form-line').removeClass('error');
},
errorPlacement: function (error, element) {
    $(element).parents('.form-group').append(error);
}
});

if(page_type == 'customer_index' || page_type == 'driver_index' || page_type == 'admin_index' || page_type == 'admin_order_edit')
{
    $('.js-basic-example').DataTable({
        responsive: true,
        order: [[ 5, "desc" ]]
    });
}

if(page_type == 'admin_order_index' || page_type == 'driver_order_index' || page_type == 'admin_dash')
{
    $('.js-basic-example').DataTable({
        responsive: true,
        order: [[ 4, "desc" ]]
    });
}

$('.deleteUserLink,.deletePDLink').on('click', function(e) {
    e.preventDefault();
    var currentElement = $(this);

    swal({
            title: "Are you sure deleting the record?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true       
        },
        function () {
            window.location.href = currentElement.attr('href');
        }
    );
});

$('.deleteOrderLink').on('click', function(e) {
    e.preventDefault();
    var currentElement = $(this);

    swal({
            title: "Are you sure deleting the Order?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true       
        },
        function () {
            window.location.href = currentElement.attr('href');
        }
    );
});


$('.confirm_order').on('click', function(e) {
    e.preventDefault();
    var currentElement = $(this);

    swal({
            title: "Are you sure want to mark this order as Completed?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "green",
            confirmButtonText: "Yes, Make it Delivered!",
            closeOnConfirm: true       
        },
        function () {
            window.location.href = currentElement.attr('href');
        }
    );
});



 Dropzone.options.frmFileUpload = {
        paramName: "file",
        uploadMultiple: false,
        createImageThumbnails: true,
        acceptedFiles: 'image/*,',
        addRemoveLinks: true,
        timeout: 5000,
        maxFiles: 12,
        thumbnailWidth: 300,
        thumbnailHeight: 300,
        init: function() {
        var myDropzone = this;
        var images = [];
        var file_count = 0;
        var max_count = 12;
        $.get(site_url+'/get/vehicle-images/'+order_uuid+'/'+vehicle_id, function (images) {

          $.each(images, function (key, image) {
            var file = {name: image.image_name,serverId:image.image_name}
            myDropzone.options.addedfile.call(myDropzone, file, site_url + '/vimages/'+ image.image_path)
            myDropzone.options.thumbnail.call(myDropzone, file, site_url + '/vimages/'+ image.image_path)
            myDropzone.emit('complete', file)
          });
          file_count = images.length;
          if(file_count == max_count)
          {
             myDropzone.disable();
          }
        });

        this.on("success", function(file, response) {
            $('.dz-image').css({"width":"100%", "height":"auto"});
          console.log(response);
          file.serverId = response;
        });
        this.on("addedfile", function(file) {
            if(file_count >= max_count){
                myDropzone.disable();
            }
            file_count++;
        });
        this.on("removedfile", function(file) {
           file_count--;
           if(file_count < max_count)
           {
             myDropzone.enable();
           }
           console.log(file);
          $.post(site_url+"/delete/vehicle-image/"+order_uuid,{file_name:file.serverId},'application/json').done(function() {
                    file.previewElement.remove();
                }); 
        });
        this.on("error", function(file, message) { 
            alert('Maximum of 12 images can be uploaded.!');
            myDropzone.disable();
            this.removeFile(file); 
       });
}

}

if(page_type == 'admin_order_create' || page_type == 'admin_order_edit')
{
    $('.livesearch').typeahead({
        items:5,
        source:  function (term, process) {
        return $.get(site_url+"/customer-search", { q: term }, function (data) {
                formatted = $.map(data, function (item) {
                        return {
                            name: item.user_uuid+"---"+item.first_name+" "+item.last_name,
                            id: item.id
                        }
                    });
                return process(formatted);
            });
        },
        afterSelect:function(){
            var curr_sel = $('.livesearch').val();
            curr_sel_arr = curr_sel.split('---');
            $('.livesearch').val(curr_sel_arr[0]);
        }
    });

    $('.livesearchDriver').typeahead({
        items:5,
        source:  function (term, process) {
        return $.get(site_url+"/driver-search", { q: term }, function (data) {
                formatted = $.map(data, function (item) {
                        return {
                            name: item.user_uuid+"---"+item.first_name+" "+item.last_name,
                            id: item.id
                        }
                    });
                return process(formatted);
            });
        },
        afterSelect:function(){
            var curr_sel = $('.livesearchDriver').val();
            curr_sel_arr = curr_sel.split('---');
            $('.livesearchDriver').val(curr_sel_arr[0]);
        }
    });

    if($('.splide').length > 0)
    {
        new Splide( '.splide', {
        type     : 'loop',
        perPage: 1,
        autoWidth: true,
        autoHeight:true,
        focus    : 'center',
        rewind   : true,
      } ).mount();
    }
   
     $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'dddd DD MMMM YYYY - HH:mm',
            clearButton: true,
            weekStart: 1
        });

    if($('#signCanvas').length)
    {
        var signaturePad = new SignaturePad(document.getElementById('signCanvas'), {
          backgroundColor: 'rgba(255, 255, 255, 0)',
          penColor: 'rgb(0, 0, 0)',
          onEnd:function(){
            //console.log(signaturePad.toDataURL());
            $('#signData').val(signaturePad.toDataURL());
            console.log($('#signData').val());
          }
        });
        var cancelButton = document.getElementById('clear');

        cancelButton.addEventListener('click', function (event) {
          signaturePad.clear();
        });
    }


}

$(document).on('click','#basic_checkbox_2',function(){
  if($("#basic_checkbox_2").is(':checked'))
  {
     $('.confirmBlock,.deliverBtnWrap').addClass('show').removeClass('hide');
  }
  else
  {
     $('.confirmBlock,.deliverBtnWrap').addClass('hide').removeClass('show');
  }
});


});