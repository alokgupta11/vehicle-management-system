@extends('layouts.app')
@section('title')
Orders List
@endsection('title')
@section('content')
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="header">
    <h2>
        Orders List
        <a href="{{url('create/order')}}" class="btn bg-indigo waves-effect pull-right">
           <b>Create Order</b>
        </a>
    </h2>
</div>

@if(session('success_msg'))
      <div class="alert alert-success alert-dismissible" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session('success_msg') }}
      </div>
    @endif

            <div class="body">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                <thead>
                    <tr>
                        <th>OrderID</th>
                        <th>Customer</th>
                        <th>Status</th>
                        <th>CreatedBy</th>
                        <th>CreatedDate</th>
                        <th>UpdatedBy</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $ord)
                    <tr>
                        <td><b>{{$ord->order_uuid}}</b></td>
                        <td><b>{{$ord->first_name}} {{$ord->last_name}}</b><br>{{$ord->customer_uuid}}
                        </td>
                        @if($ord->order_status == 0)
                        <td><h5><label class="label bg-orange">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Driver Assigned')
                        <td><h5><label class="label bg-cyan">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Picked')
                        <td><h5><label class="label bg-orange">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Delivered')
                        <td><h5><label class="label bg-green">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Delivered by Driver')
                        <td><h5><label class="label bg-indigo">{{$ord->status}}</label></h5></td>
                        @elseif($ord->status == 'Delivered- Waiting to Reassign')
                        <td><h5><label class="label bg-orange">{{$ord->status}}</label></h5></td>
                        @else
                        <td></td>
                        @endif
                        <td>{{$ord->created_by}}</td>
                        <td>{{$ord->created_at}}</td>
                        <td>{{$ord->updated_by}}</td>
                        <td>

                           <!--  <a href="{{url('edit/order/'.$ord->order_uuid.'?edit=pick-drop')}}" class="btn bg-blue btn-xs waves-effect"><i class="material-icons">person_add</i></a> -->
                            @if($ord->first_driver == '' )
                             <a href="{{url('order/package/'.$ord->order_uuid.'?act=add')}}" class="btn bg-blue btn-xs waves-effect" title="Assign Package to Driver"><i class="material-icons" title="Assign Package to Driver">person_add</i></a>
                             @else

                                @if($ord->order_status == 3)
                                 <a href="{{url('order/reassign/'.$ord->order_uuid.'?act=reasign')}}" class="btn bg-orange btn-xs waves-effect" title="Re-Assign Package to Driver"><i class="material-icons" title="Re-Assign Package to Driver">person_add</i></a>
                                @endif 
                             @endif

                             <a href="{{url('order/history/'.$ord->order_uuid.'')}}" class="btn bg-green btn-xs waves-effect" title="View history"><i class="material-icons" title="View history">visibility</i></a>

                            <a href="{{url('edit/order/'.$ord->order_uuid.'?act=edit-order')}}" class="btn bg-cyan btn-xs waves-effect"><i class="material-icons">edit</i></a>

                            @if($ord->order_status != 5)
                                <a href="{{url('confirm_order/order/'.$ord->order_uuid)}}" class="btn bg-green btn-xs waves-effect confirm_order" data-type="confirm" title="Confirm Delivery"><i class="material-icons" title="Confirm Delivery">check_circle</i></a>
                            @endif

                            <a href="{{url('delete/order/'.$ord->order_uuid)}}" class="btn bg-red btn-xs waves-effect deleteOrderLink" data-type="confirm"><i class="material-icons">delete</i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
</div>
</div>
</div>
@endsection('content')