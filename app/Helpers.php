<?php
use Illuminate\Support\Str;
use App\Models\OrderHistory;
use Illuminate\Support\Facades\Auth;

if(!function_exists('unique_random_hash'))
{
	function unique_random_hash($table, $col, $chars = 13){
        $unique = false;
        // Store tested results in array to not test them again
        $tested = [];
        do{
            $random = Str::random($chars);
            if( in_array($random, $tested) ){
                continue;
            }
            $count = DB::table($table)->where($col, '=', $random)->count();
            $tested[] = $random;
            if( $count == 0){
                $unique = true;
            }
        }
        while(!$unique);
        return strtoupper($random);
    }
}

if(!function_exists('current_user'))
{
    function current_user(){
        return Auth::user()->first_name." ".Auth::user()->last_name;
    }
}

if(!function_exists('activity_logg'))
{
    function activity_logg($data){
        return OrderHistory::create($data);
    }
}

function areActiveRoutes(Array $routes, $output = "active")
{
    foreach ($routes as $route)
    {
        if (Route::currentRouteName() == $route) return $output;
    }
}
