@extends('layouts.app')
@section('title')
Order Create
@endsection('title')
@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
            	<a href="{{url('orders')}}" class="btn bg-indigo waves-effect pull-right">
                   <b> Orders List</b>
                </a>
                <h2>Create Order</h2>
            </div>
            <div class="body">
            	@if(session('success_msg'))
		          <div class="alert alert-success alert-dismissible" role="alert">
		             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		              {{ session('success_msg') }}
		          </div>
			    @endif

			    @if(session('error_msg'))
		          <div class="alert alert-danger alert-dismissible" role="alert">
		             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		              {{ session('error_msg') }}
		          </div>
			    @endif


            	@if ($errors->any())
			    <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif
                <form id="form_validation" method="POST" action="@if(request()->act == 'edit-order') {{route('admin_order_update_details')}} @else {{route('admin_order_save')}} @endif">
                	@csrf
                	<div class="row">
	                	<div class="col-md-6">
	                	<div class="form-group form-float">
	                        <div class="form-line">
	                        	@if(request()->act == "edit-order")
	                            	<input type="text" class="form-control" name="order_uuid" required value="{{$order->order_uuid}}" 	readonly="">
	                            @else
	                            	<input type="text" class="form-control" name="order_uuid" required value="{{$order_uuid}}" readonly="">
	                            @endif
	                            <b><label class="form-label">Order ID</label></b>
	                        </div>
	                    </div>
	                    </div>
                    
                		<div class="col-md-6">
	                    <div class="form-group form-float">
	                        <div class="form-line">
	                        	 <label class="form-label">Enter Customer ID</label>

	                        	 @if(request()->act == "edit-order")
	                            	<input type="text" class="livesearch form-control" name="customer_uuid" required value="{{$order->customer_uuid}}" 	readonly="">
	                            @else
	                            	<input type="text" class="livesearch form-control" name="customer_uuid" required value="{{old('customer_uuid')}}" >
	                            @endif

	                        </div>
	                    </div>
	                   </div>
                   </div>



                   <h3>Vehicle Details</h3>
                   <hr>
                   <div class="row">
		               <div class="col-md-4">
		                <div class="">
		                    <div class="">
		                        <!-- <label class="form-label">Year</label> -->
		                        <select class="form-control show-tick" name="year">
		                            <option value="0">Select Year</option>
		                            <option value="2021" {{ isset($vehicle->year) && $vehicle->year == "2021" ? 'selected':'' }}>2021</option>
		                            <option value="2020" {{ isset($vehicle->year) && $vehicle->year == "2020" ? 'selected':'' }}>2020</option>
		                            <option value="2019" {{ isset($vehicle->year) && $vehicle->year == "2019" ? 'selected':'' }}>2019</option>
		                            <option value="2018" {{ isset($vehicle->year) && $vehicle->year == "2018" ? 'selected':'' }}>2018</option>
		                            <option value="2017" {{ isset($vehicle->year) && $vehicle->year == "2017" ? 'selected':'' }}>2017</option>
		                            <option value="2016" {{ isset($vehicle->year) && $vehicle->year == "2016" ? 'selected':'' }}>2016</option>
		                            <option value="2015" {{ isset($vehicle->year) && $vehicle->year == "2015" ? 'selected':'' }}>2015</option>
		                        </select>
		                    </div>
		                </div>
		                </div>

		                <div class="col-md-4">
		                 <div class="">
		                    <div class="">
		                        <!-- <label class="form-label">Year</label> -->
		                        <select class="form-control show-tick" name="condition">
		                            <option value="0">Select Condition</option>
		                            <option value="OLD" {{ isset($vehicle->condition) && $vehicle->condition == "OLD" ? 'selected':'' }}>OLD</option>
		                            <option value="NEW" {{ isset($vehicle->condition) && $vehicle->condition == "NEW" ? 'selected':'' }}>NEW</option>
		                            <option value="DAMAGED" {{ isset($vehicle->condition) && $vehicle->condition == "DAMAGED" ? 'selected':'' }}>DAMAGED</option>
		                        </select>
		                    </div>
		                </div>
		                </div>

		                <div class="col-md-4">
		                <div class="">
		                    <div class="">
		                        <!-- <label class="form-label">Year</label> -->
		                        <select class="form-control show-tick" name="is_keys">
		                            <option value="0">Is Keys Present</option>
		                            <option value="YES" {{ isset($vehicle->is_keys) && $vehicle->is_keys == "YES" ? 'selected':'' }}>Yes</option>
		                            <option value="NO" {{ isset($vehicle->is_keys) && $vehicle->is_keys == "NO" ? 'selected':'' }}>NO</option>
		                        </select>
		                    </div>
		                </div>
		                </div>

		            </div>
		            <div class="row">
		                <div class="col-md-6">
		                <div class="form-group form-float">
		                    <div class="form-line">
		                        <input type="text" class="form-control" name="model" value="{{isset($vehicle->model) ? $vehicle->model : old('model')}}">
		                        <label class="form-label">Model</label>
		                    </div>
		                </div>
		                </div>
		                 <div class="col-md-6">
		                <div class="form-group form-float">
		                    <div class="form-line">
		                        <input type="text" class="form-control" name="vin" value="{{isset($vehicle->vin) ? $vehicle->vin : old('vin')}}">
		                        <label class="form-label">VIN</label>
		                    </div>
		                </div>
		                </div>
		            </div>
		            <div class="row">


		                 <div class="col-md-6">
		                <div class="form-group form-float">
		                    <div class="form-line">
		                        <input type="text" class="form-control" name="make" value="{{isset($vehicle->make) ? $vehicle->make : old('make')}}">
		                        <label class="form-label">Make</label>
		                    </div>
		                </div>
		                </div>

		                <div class="col-md-6">
		                <div class="form-group form-float">
		                    <div class="form-line">
		                        <input type="text" class="form-control" name="stock" value="{{isset($vehicle->stock) ? $vehicle->stock : old('stock')}}">
		                        <label class="form-label">Stock</label>
		                    </div>
		                </div>
		                </div>
		                 
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                <div class="form-group form-float">
		                    <div class="form-line">
		                        <textarea name="comment" cols="30" rows="3" class="form-control no-resize">{{isset($vehicle->comment) ? $vehicle->comment : old('comment')}}</textarea>
		                        <label class="form-label">Comment</label>
		                    </div>
		                </div>
		               </div>
		           </div>




                   <h3>Package Pickup & Destination Details</h3>
                   <hr>

                   <div class="row">
	                   	<div class="col-md-6">
	                   		<div class="panel panel-primary">
							  <div class="panel-heading" style="text-align: center;font-size: 20px;">Pickup Details</div>
							  <div class="panel-body">
							  	<div class="row">
					                <div class="col-md-12">
					                <div class="form-group form-float">
					                    <div class="form-line">
					                        <input type="text" class="form-control" name="from_location" value="{{isset($order->from_location) ? $order->from_location : old('from_location')}}">
					                        <label class="form-label">Pickup Location</label>
					                    </div>
					                </div>
					                </div>
					               <div class="col-md-12">
					                <div class="form-group form-float">
					                    <div class="form-line">
					                        <input type="text" class="form-control" name="from_address" value="{{isset($order->from_address) ? $order->from_address : old('from_address')}}">
					                        <label class="form-label">Pickup Address</label>
					                    </div>
					                </div>
					                </div>

					                <div class="col-md-12">
					                <div class="form-group form-float">
					                    <div class="form-line">
					                        <input type="text" class="form-control" name="from_zipcode" value="{{isset($order->from_zipcode) ? $order->from_zipcode : old('from_zipcode')}}">
					                        <label class="form-label">Pickup Zip Code</label>
					                    </div>
					                </div>
					                </div>

					            </div>

							  </div>
							</div>
	                   	</div>


	                   	<div class="col-md-6">
                   		<div class="panel panel-primary" style="    border-color: #008000;">
						  <div class="panel-heading" style="text-align: center;font-size: 20px;background-color: green;">Final Destination Details</div>
						  <div class="panel-body">
						  	<div class="row">
				                <div class="col-md-12">
				                <div class="form-group form-float">
				                    <div class="form-line">
				                        <input type="text" class="form-control" name="to_location" value="{{isset($order->to_location) ? $order->to_location : old('to_location')}}">
				                        <label class="form-label">To Location</label>
				                    </div>
				                </div>
				                </div>
				               <div class="col-md-12">
				                <div class="form-group form-float">
				                    <div class="form-line">
				                         <input type="text" class="form-control" name="to_address" value="{{isset($order->to_address) ? $order->to_address : old('to_address')}}">
				                        <label class="form-label">To Address</label>
				                    </div>
				                </div>
				                </div>

				                <div class="col-md-12">
				                <div class="form-group form-float">
				                    <div class="form-line">
				                        <input type="text" class="form-control" name="to_zipcode" value="{{isset($order->to_zipcode) ? $order->to_zipcode : old('to_zipcode')}}">
				                        <label class="form-label">Destination ZipCode</label>
				                    </div>
				                </div>
				                </div>

				            </div>

						  </div>
						</div>
                   	</div>
                   </div>

                 @if(request()->act == "edit-order")
                   <center><button class="btn btn-warning waves-effect" name="action" value="update-order" type="submit" style="font-size: 20px">Update Order</button></center>
                 @else
                 	<center><button class="btn btn-primary waves-effect"  name="action" value="submit-order" type="submit" style="font-size: 20px">Create Order</button></center>
                 @endif
                    
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
@endsection('content')


<style type="text/css">
	.bootstrap-select.btn-group .dropdown-toggle .filter-option {
    display: inline-block;
    overflow: hidden;
    width: 100%;
    text-align: left;
    font-size: 15px;
    margin-left: -10px;
}
</style>