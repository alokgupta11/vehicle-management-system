<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_uuid')->nullable();
            $table->string('customer_uuid');
            $table->string('delivery_person')->nullable();
            $table->string('receiver')->nullable();
            $table->string('condition')->nullable();
            $table->string('signature',500)->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->string('created_by_id')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by_id')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
