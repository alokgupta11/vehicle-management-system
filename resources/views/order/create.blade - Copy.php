@extends('layouts.app')
@section('title')
Order Create
@endsection('title')
@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
            	<a href="{{url('orders')}}" class="btn bg-indigo waves-effect pull-right">
                   <b> Orders List</b>
                </a>
                <h2>Create Order</h2>
            </div>
            <div class="body">
            	@if(session('success_msg'))
		          <div class="alert alert-success alert-dismissible" role="alert">
		             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		              {{ session('success_msg') }}
		          </div>
			    @endif
            	@if ($errors->any())
			    <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif
                <form id="form_validation" method="POST" action="{{route('admin_order_save')}}">
                	@csrf
                	<div class="row">
	                	<div class="col-md-6">
	                	<div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="text" class="form-control" name="order_uuid" required value="{{$order_uuid}}">
	                            <b><label class="form-label">Order ID</label></b>
	                        </div>
	                    </div>
	                    </div>
                    </div>
                	<div class="row">
                		<div class="col-md-6">
	                    <div class="form-group form-float">
	                        <div class="form-line">
	                        	 <label class="form-label">Enter Customer ID</label>
	                             <input class="livesearch form-control" type="text" name="customer_uuid" required value="{{old('customer_uuid')}}">
	                        </div>
	                    </div>
	                   </div>
                   </div>
                   <!-- <div class="row">
                		<div class="col-md-6">
	                    <div class="form-group form-float">
	                    	<div>Select Delivery type:</div>
	                    	<br>
	                        <div class="">
	                            <input name="type" id="radio_30" type="radio" class="with-gap radio-col-cyan" value="single" checked="checked">
                                <label for="radio_30">Single Location</label>
	                            <input name="type" id="radio_31" type="radio" class="with-gap radio-col-cyan" value="multiple">
	                            <label for="radio_31">Multiple Location</label>
	                        </div>
	                    </div>
	                   </div>
                   </div> -->
                    <button class="btn btn-primary waves-effect" type="submit">Create Order</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
@endsection('content')