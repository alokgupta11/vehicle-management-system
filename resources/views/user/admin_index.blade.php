@extends('layouts.app')
@section('title')
Admins
@endsection('title')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <a href="{{url('create/admin')}}" class="btn bg-indigo waves-effect pull-right">
                   <b>Add Admin</b>
                </a>
                <h2>Admins List</h2>
            </div>
            <div class="body">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                <thead>
                    <tr>
                        <th>AdminID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>CreatedDate</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($admins as $adm)
                    <tr>
                        <td>{{$adm->user_uuid}}</td>
                        <td>{{$adm->first_name}} {{$adm->last_name}}</td>
                        <td>{{$adm->phone}}</td>
                        <td>{{$adm->email}}</td>
                        <td>{{$adm->address}}</td>
                        <td>{{$adm->created_at}}</td>
                        <td>
                            <a href="{{url('edit/admin/'.$adm->user_uuid)}}" class="btn bg-cyan btn-xs waves-effect"><i class="material-icons">edit</i></a>
                            <a href="{{url('delete/admin/'.$adm->user_uuid)}}" class="btn bg-red btn-xs waves-effect deleteUserLink" data-type="confirm"><i class="material-icons">delete</i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
</div>
</div>
</div>
@endsection('content')