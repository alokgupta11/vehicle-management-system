<!DOCTYPE html>
<html>
<head>
    @include('partials.header')
</head>
<body class="theme-light-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    @include('partials.topbar')
    <section>
        <!-- Left Sidebar -->
        @include('partials.sidebar_left')
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        @include('partials.sidebar_right')
        <!-- #END# Right Sidebar -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>DASHBOARD</h2> -->
                <br>
                @yield('content')
            </div>
        </div>
    </section>
    @include('partials.footer')
</body>
</html>
