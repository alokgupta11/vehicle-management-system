<aside id="leftsidebar" class="sidebar">
<!-- User Info -->
<div class="user-info">
    <div class="image text-center">
        @if(!is_null(Auth::user()->profile_path) && Auth::user()->profile_path != "")
        <img src="{{asset('pimages/'.Auth::user()->profile_path)}}" width="48" height="48" alt="User" />
        @else
        <img src="{{asset('pimages/default.png')}}" width="48" height="48" alt="User" />
        @endif
        
    </div>
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome <b>{{Auth::user()->first_name}}!, </b>
        @if(Auth::user()->role_id == '1')
        <i>Admin</i>
        @elseif(Auth::user()->role_id == '2')
        <i>Driver</i>
        @endif
        </div>
        <div class="email">{{Auth::user()->email}}</div>
        <div class="btn-group user-helper-dropdown">
            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
            <ul class="dropdown-menu pull-right">
                <li><a href="{{url('profile')}}"><i class="material-icons">person</i>My Profile</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{url('edit-password')}}"><i class="material-icons">settings</i>Edit Password</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{url('logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- #User Info -->
<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{ areActiveRoutes(['admin_dash','user_dash','home_page']) }}">
            <a href="{{url('/')}}">
                <i class="material-icons">home</i>
                <span>Home</span>
            </a>
        </li>
        @if(Auth::user()->role_id == '1')
        <li class="{{ areActiveRoutes(['admin_order_index','admin_order_create','admin_order_edit']) }}">
            <a href="{{url('orders')}}">
                <i class="material-icons">layers</i>
                <span>Orders List</span>
            </a>
        </li>
        <li class="{{ areActiveRoutes(['customer_index','customer_create','customer_edit']) }}">
            <a href="{{url('customers')}}">
                <i class="material-icons">supervised_user_circle</i>
                <span>Customers List</span>
            </a>
        </li>
        <li class="{{ areActiveRoutes(['driver_index','driver_create','driver_edit']) }}">
            <a href="{{url('drivers')}}">
                <i class="material-icons">supervised_user_circle</i>
                <span>Drivers List</span>
            </a>
        </li>
        <li class="{{ areActiveRoutes(['admin_index','admin_create','admin_edit']) }}">
            <a href="{{url('admins')}}">
                <i class="material-icons">supervised_user_circle</i>
                <span>Admins List</span>
            </a>
        </li>
        @endif
        @if(Auth::user()->role_id == '2')
        <li class="{{ areActiveRoutes(['driver_order_index','admin_order_create','admin_order_edit']) }}">
            <a href="{{url('my-orders')}}">
                <i class="material-icons">layers</i>
                <span>My Orders List</span>
            </a>
        </li>
        @endif
        <li>
            <a href="{{url('logout')}}">
                <i class="material-icons">input</i>
                <span>Logout</span>
            </a>
        </li>
    </ul>
</div>
<!-- #Menu -->
<!-- Footer -->
<div class="legal">
    <div class="copyright">
        &copy; 2021 <a href="javascript:void(0);">{{ config('app.name') }}</a>.
    </div>
    <div class="version">
        <b>Version: </b> 1.0.5
    </div>
</div>
<!-- #Footer -->
</aside>