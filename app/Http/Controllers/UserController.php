<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\UserInterface;
use Illuminate\Http\Request;
use Validator;
use Session;
use Hash;
use Illuminate\Support\Facades\Auth;
use Image;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserInterface $user)
    {
        $this->middleware('auth');
        $this->user = $user;
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function account()
    {
        $pageType = 'profile_edit';
        $user = Auth::user();
        return view('edit_profile',compact('pageType','user'));
    }

    public function updateAccount(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $validator = Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'phone' => 'required|unique:users,phone,'.$user->id,
            'address' => 'required'
        ]);
        if ($validator->fails()) {
        Session::flash('error_msg','Please check the errors.');
        return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        unset($data['_token']);
        if($request->hasFile('profile_image'))
        {
            $image = $request->file('profile_image');
            if ($image->isValid()) {
                $image_name = $user->user_uuid.".". $image->getClientOriginalExtension();
                //$base_path = base_path().'/public/pimages/';
                $db_path = $image_name;
                //$path = base_path().'/public/pimages/';
                //$image->move($path,$image_name);

                $canvas = Image::canvas(245, 245);
                $image_int  = Image::make($image->getRealPath())->resize(245, 245, function($constraint)
                {
                    $constraint->aspectRatio();
                });

                $canvas->insert($image_int, 'center');
                $canvas->save('pimages/'.$image_name);

                unset($data['profile_image']);
                $data['profile_path'] = $db_path;
            }
        }

        $user = $this->user->updateUser($user->user_uuid,$data);
        Session::flash('success_msg','Profile details updated successfully.');
        return redirect()->back();
    }

    public function changePassword()
    {
        $pageType = 'change_password';
        return view('edit_password',compact('pageType'));
    }

    public function updatePassword(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $validator = Validator::make($data, [
            'password' => "required|min:8|confirmed",
        ]);
        if ($validator->fails()) {
        return redirect()->back()->withErrors($validator);
        }
        unset($data['_token']);
        unset($data['password_confirmation']);
        $data['password'] = Hash::make($data['password']);
        $this->user->updateUser($user->user_uuid,$data);
        Session::flash('success_msg','Password updated successfully.');
        return redirect()->back();
    }
}