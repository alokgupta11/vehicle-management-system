<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\UserInterface;
use App\Repositories\Interfaces\OrderInterface;
use Illuminate\Http\Request;
use Validator;
use Session;
use Hash;
use DB;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserInterface $user,OrderInterface $order)
    {
        //$this->middleware('admin');
        $this->user = $user;
        $this->order = $order;
    }

    public function index()
    {
        $pageType = 'admin_dash';
        $stats = $this->order->getAdminStats();
        $order_history = $this->order->getOrderHistory();
        //return $order_history;
        return view('admin_dashboard',compact('pageType','stats','order_history'));
    }

    public function listCustomers()
    {
       $pageType = 'customer_index';
       $customers = $this->user->getAllCustomers();
       //return $customers;
       return view('user.customer_index',compact('pageType','customers'));
    }

    public function createCustomer()
    {
        $pageType = 'customer_create';
        $user_uuid = unique_random_hash('users','user_uuid',3);
        return view('user.customer_create',compact('pageType','user_uuid'));
    }

    public function saveCustomer(Request $request)
    {
        $data = $request->all();
        $rules = [
            'user_uuid' => 'required|size:3',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users',
            'address' => 'required',
            'password' => "required|min:8|confirmed",
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        unset($data['_token']);
        $data['email_verified_at'] = now();
        $data['role_id'] = '3';
        $data['profile_path'] = '';
        $data['password'] = Hash::make($data['password']);
        $data['last_edit_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
        $user = $this->user->createUser($data);
        
        Session::flash('success_msg','Customer created successfully.');
        return redirect()->to('customers');
    }

    public function editCustomer($uuid)
    {
        $pageType = 'customer_edit';
        $customer = $this->user->getUserByUUID($uuid);
        return view('user.customer_edit',compact('pageType','customer'));
    }

    public function updateCustomer($uuid,Request $request)
    {
        $data = $request->all();
        $id = $this->user->getUserIDByUUID($uuid);
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$id,
            'phone' => 'required|unique:users,phone,'.$id,
            'address' => 'required'
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        unset($data['_token']);
        $data['last_edit_by'] = Auth::user()->first_name." ".Auth::user()->last_name;
        $result = $this->user->updateUser($uuid,$data);
        Session::flash('success_msg','Customer updated successfully.');
        return redirect()->back();
    }

    public function deleteCustomer($uuid)
    {
        $user = $this->user->deleteUserByUUID($uuid);
        Session::flash('success_msg','Customer deleted successfully.');
        return redirect()->back();
    }

    public function listDrivers()
    {
       $pageType = 'driver_index';
       $drivers = $this->user->getAllDrivers();
       //return $customers;
       return view('user.driver_index',compact('pageType','drivers'));
    }

    public function createDriver()
    {
        $pageType = 'driver_create';
        $user_uuid = unique_random_hash('users','user_uuid',10);
        return view('user.driver_create',compact('pageType','user_uuid'));
    }

    public function saveDriver(Request $request)
    {
        $data = $request->all();
        $rules = [
            'user_uuid' => 'required|size:10',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users',
            'address' => 'required',
            'password' => "required|min:8|confirmed",
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        unset($data['_token']);
        $data['email_verified_at'] = now();
        $data['role_id'] = '2';
        $data['profile_path'] = '';
        $data['password'] = Hash::make($data['password']);
        $user = $this->user->createUser($data);
        
        Session::flash('success_msg','Driver created successfully.');
        return redirect()->to('drivers');
    }

    public function editDriver($uuid)
    {
        $pageType = 'driver_edit';
        $driver = $this->user->getUserByUUID($uuid);
        return view('user.driver_edit',compact('pageType','driver'));
    }

    public function updateDriver($uuid,Request $request)
    {
        $data = $request->all();
        $id = $this->user->getUserIDByUUID($uuid);
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$id,
            'phone' => 'required|unique:users,phone,'.$id,
            'address' => 'required'
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        unset($data['_token']);
        $result = $this->user->updateUser($uuid,$data);
        Session::flash('success_msg','Driver updated successfully.');
        return redirect()->back();
    }

    public function deleteDriver($uuid)
    {
        $user = $this->user->deleteUserByUUID($uuid);
        Session::flash('success_msg','Driver deleted successfully.');
        return redirect()->back();
    }

    public function listAdmins()
    {
       $pageType = 'admin_index';
       $admins = $this->user->getAllAdmins();
       //return $customers;
       return view('user.admin_index',compact('pageType','admins'));
    }

    public function createAdmin()
    {
        $pageType = 'admin_create';
        $user_uuid = unique_random_hash('users','user_uuid',10);
        return view('user.admin_create',compact('pageType','user_uuid'));
    }

    public function saveAdmin(Request $request)
    {
        $data = $request->all();
        $rules = [
            'user_uuid' => 'required|size:10',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users',
            'address' => 'required',
            'password' => "required|min:8|confirmed",
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        unset($data['_token']);
        $data['email_verified_at'] = now();
        $data['role_id'] = '1';
        $data['profile_path'] = '';
        $data['password'] = Hash::make($data['password']);
        $user = $this->user->createUser($data);
        
        Session::flash('success_msg','Admin created successfully.');
        return redirect()->to('admins');
    }

    public function editAdmin($uuid)
    {
        $pageType = 'admin_edit';
        $admin = $this->user->getUserByUUID($uuid);
        return view('user.admin_edit',compact('pageType','admin'));
    }

    public function updateAdmin($uuid,Request $request)
    {
        $data = $request->all();
        $id = $this->user->getUserIDByUUID($uuid);
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$id,
            'phone' => 'required|unique:users,phone,'.$id,
            'address' => 'required'
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        unset($data['_token']);
        $result = $this->user->updateUser($uuid,$data);
        Session::flash('success_msg','Admin updated successfully.');
        return redirect()->back();
    }

    public function deleteAdmin($uuid)
    {
        $user = $this->user->deleteUserByUUID($uuid);
        Session::flash('success_msg','Admin deleted successfully.');
        return redirect()->back();
    }

    public function getCustomersSearchList($search)
    {
        $customers = $this->user->getCustomersSearchList($search);
        return $customers;
    }
}