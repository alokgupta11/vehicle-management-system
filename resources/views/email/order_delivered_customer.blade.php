@component('mail::message')

<p>Dear Customer <b>{{$user->first_name}} {{$user->last_name}}!</b></p>

<p>Your Order <b>#{{$order->order_uuid}}</b> has been successfully delivered on {{$order->updated_at}}</p>

<p>If any queries reach us @ <b>Phone: </b>{{env('SUPPORT_CONTACT')}}</b> <b>Email: </b>{{env('SUPPORT_EMAIL')}}
</b></p>
<p></p>
Thanks & Regards,<br>
The Support team,<br>
<b>{{ config('app.name') }}</b>
@endcomponent