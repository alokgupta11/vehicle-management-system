@extends('layouts.app')
@section('title')
Drivers
@endsection('title')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <a href="{{url('create/driver')}}" class="btn bg-indigo waves-effect pull-right">
                   <b>Add Driver</b>
                </a>
                <h2>Drivers List</h2>
            </div>
            <div class="body">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                <thead>
                    <tr>
                        <th>DriverID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>CreatedDate</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($drivers as $driv)
                    <tr>
                        <td>
                        <p>{{$driv->user_uuid}}</p>
                        @if(!is_null($driv->profile_path) && $driv->profile_path !="")
                        <img src="{{asset('pimages/'.$driv->profile_path)}}" width="120" height="120" alt="User" />
                        @else
                        <img src="{{asset('pimages/default.png')}}" width="120" height="120" alt="User" />
                        @endif
                        </td>
                        <td>{{$driv->first_name}} {{$driv->last_name}}</td>
                        <td>{{$driv->phone}}</td>
                        <td>{{$driv->email}}</td>
                        <td>{{$driv->address}}</td>
                        <td>{{$driv->created_at}}</td>
                        <td>
                            <a href="{{url('edit/driver/'.$driv->user_uuid)}}" class="btn bg-cyan btn-xs waves-effect"><i class="material-icons">edit</i></a>
                            <a href="{{url('delete/driver/'.$driv->user_uuid)}}" class="btn bg-red btn-xs waves-effect deleteUserLink" data-type="confirm"><i class="material-icons">delete</i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
</div>
</div>
</div>
@endsection('content')