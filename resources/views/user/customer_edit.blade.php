@extends('layouts.app')
@section('title')
Customer Edit
@endsection('title')
@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
            	<a href="{{url('customers')}}" class="btn bg-indigo waves-effect pull-right">
                   <b>Customers List </b>
                </a>
                <h2>Customer Edit - {{$customer->first_name}} {{$customer->last_name}}</h2>
            </div>
            <div class="body">
            	@if(session('success_msg'))
		          <div class="alert alert-success alert-dismissible" role="alert">
		             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		              {{ session('success_msg') }}
		          </div>
			    @endif
            	@if ($errors->any())
			    <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif
                <form id="form_validation" method="POST" action="{{route('customer_update',$customer->user_uuid)}}">
                	@csrf
                	<div class="row">
	                	<div class="col-md-6">
	                	<div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="text" class="form-control" name="user_uuid" value="{{$customer->user_uuid}}" disabled="disabled">
	                            <label class="form-label">Customer ID</label>
	                        </div>
	                    </div>
	                    </div>
                    </div>
                	<div class="row">
                		<div class="col-md-6">
	                    <div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="text" class="form-control" name="first_name" required  value="{{$customer->first_name}}">
	                            <label class="form-label">First Name</label>
	                        </div>
	                    </div>
	                   </div>
	                   <div class="col-md-6">
	                    <div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="text" class="form-control" name="last_name" required value="{{$customer->last_name}}">
	                            <label class="form-label">Last Name</label>
	                        </div>
	                    </div>
	                  </div>
                   </div>
                   <div class="row">
                		<div class="col-md-6">
	                    <div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="text" class="form-control" name="email" required value="{{$customer->email}}">
	                            <label class="form-label">Email</label>
	                        </div>
	                    </div>
	                   </div>
	                   <div class="col-md-6">
	                    <div class="form-group form-float">
	                        <div class="form-line">
	                            <input type="text" class="form-control" name="phone" required value="{{$customer->phone}}">
	                            <label class="form-label">Phone</label>
	                        </div>
	                    </div>
	                  </div>
                   </div>
         
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea name="address" cols="30" rows="3" class="form-control no-resize" required>{{$customer->address}}</textarea>
                            <label class="form-label">Address</label>
                        </div>
                    </div>
                    <button class="btn btn-primary waves-effect" type="submit">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
@endsection('content')