<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Storage;
use DB;
use Cache;
use App;
use Response;
use App\Models\User;
use Auth;
use Log;
use Artisan;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    
    }

    public function hello()
    {
       //echo unique_random_hash('users','user_uuid', 10);
        $exitCode = Artisan::call('route:cache');
        $exitCode = Artisan::call('config:cache');
        $exitCode = Artisan::call('cache:clear');
        $exitCode = Artisan::call('view:clear');
    }
}
