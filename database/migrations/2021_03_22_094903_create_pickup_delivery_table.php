<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickupDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pickup_delivery', function (Blueprint $table) {
            $table->id();
            $table->string('order_id')->nullable();
            $table->string('driver_uuid')->nullable();
            $table->string('is_final_destination')->nullable();
            $table->string('pickup_location')->nullable();
            $table->text('pickup_address')->nullable();
            $table->string('pickup_zip')->nullable();
            $table->string('pickup_time')->nullable();
            $table->string('delivery_location')->nullable();
            $table->text('delivery_address')->nullable();
            $table->string('delivery_zip')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('status')->nullable();
            $table->string('created_by_id')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by_id')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pickup_delivery');
    }
}
