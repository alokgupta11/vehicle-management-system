@extends('layouts.app')
@section('title')
Admin Dashboard
@endsection('title')
@section('content')
<h4 class="">Admin Dashboard</h4>
<br>
<div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-blue hover-expand-effect">
            <div class="icon">
                <i class="material-icons">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text">Orders Total</div>
                <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20">{{$stats['order_total']}}</div>
            </div>
        </div>
    </div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-light-green hover-expand-effect">
            <div class="icon">
                <i class="material-icons">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text">Orders Delivered</div>
                <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20">{{$stats['order_delivered']}}</div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-orange hover-expand-effect">
            <div class="icon">
                <i class="material-icons">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text">Orders Pending</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">{{$stats['order_pending']}}</div>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="info-box bg-cyan hover-expand-effect">
        <div class="icon">
            <i class="material-icons">person_add</i>
        </div>
        <div class="content">
            <div class="text">Customers</div>
            <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20">{{$stats['customer_count']}}</div>
        </div>
    </div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="info-box bg-teal hover-expand-effect">
        <div class="icon">
            <i class="material-icons">person_add</i>
        </div>
        <div class="content">
            <div class="text">Drivers</div>
            <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20">{{$stats['driver_count']}}</div>
        </div>
    </div>
</div>
</div>
<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="header">
    <h2>
        Order History
    </h2>
</div>
            <div class="body">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>OrderID</th>
                        <th>Action</th>
                        <th>Message</th>
                        <th>Datetime</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($order_history as $oh)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><b>{{$oh->order_id}}</b></td>
                        @if($oh->action == 'CREATE')
                        <td><label class="label bg-green">{{$oh->action}}</label></td>
                        @elseif($oh->action == 'DELETE')
                        <td><label class="label bg-red">{{$oh->action}}</label></td>
                        @else
                        <td><label class="label bg-cyan">{{$oh->action}}</label></td>
                        @endif
                        <td><b>{{$oh->message}}</b></td>
                        <td>{{$oh->created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
</div>
</div>
</div>
@endsection('content')